package dawsoncollege.android.pokemac

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */

/**
 * Data class for when we fetch the available moves for a Pokemon
 */
data class MoveQuery(
    val name: String,
    val level: Int
)
