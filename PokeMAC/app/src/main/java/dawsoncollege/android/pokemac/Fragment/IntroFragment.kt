package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.Database.PokemonDatabase
import dawsoncollege.android.pokemac.Database.getDb
import dawsoncollege.android.pokemac.Database.loadPokemonByName
import dawsoncollege.android.pokemac.R
import dawsoncollege.android.pokemac.databinding.FragmentIntroBinding
import dawsoncollege.android.pokemac.getBitmap
import dawsoncollege.android.pokemac.setPokemonStarter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class IntroFragment : Fragment(R.layout.fragment_intro) {

    // Fields
    private lateinit var binding: FragmentIntroBinding
    private lateinit var db: PokemonDatabase

    /**
     * Creates the view and return the root binding (default function).
     */

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = FragmentIntroBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Lets the user choose a starter Pokemon and hes name.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Database part to save the first Pokemon.
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireContext().applicationContext)

            withContext(Dispatchers.Main){
                bindingEvent()
            }
        }

        // Binding for the fragments.
        val name1 = binding.pokemon1.text.toString()
        val name2 = binding.pokemon2.text.toString()
        val name3 = binding.pokemon3.text.toString()

        lifecycleScope.launch(Dispatchers.IO){
            loadPokemonByName(name1, requireContext().applicationContext)
            loadPokemonByName(name2, requireContext().applicationContext)
            loadPokemonByName(name3, requireContext().applicationContext)

            val front1Data = db.PokemonDao().getPokemonByName(name1)?.frontSprite!!
            val front2Data = db.PokemonDao().getPokemonByName(name2)?.frontSprite!!
            val front3Data = db.PokemonDao().getPokemonByName(name3)?.frontSprite!!

            val front1 = getBitmap(front1Data)
            val front2 = getBitmap(front2Data)
            val front3 = getBitmap(front3Data)
            withContext(Dispatchers.Main){
                binding.image1.setImageBitmap(front1)
                binding.image2.setImageBitmap(front2)
                binding.image3.setImageBitmap(front3)
            }
        }
    }


    /**
     * When the user clicks on a Pokemon it will save it as hes first Pokemon.
     */
    private suspend fun bindingEvent(){
        binding.image1.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                val name = binding.pokemon1.text.toString()
                setPokemonStarter(name, requireContext().applicationContext)
                withContext(Dispatchers.Main){
                    it.findNavController().navigate(R.id.action_introFragment_to_menuFragment)
                }
            }
        }
        binding.image2.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                val name = binding.pokemon2.text.toString()
                setPokemonStarter(name, requireContext().applicationContext)
                withContext(Dispatchers.Main){
                    it.findNavController().navigate(R.id.action_introFragment_to_menuFragment)
                }
            }
        }
        binding.image3.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                val name = binding.pokemon2.text.toString()
                setPokemonStarter(name, requireContext().applicationContext)
                withContext(Dispatchers.Main){
                    it.findNavController().navigate(R.id.action_introFragment_to_menuFragment)
                }
            }
        }
    }
}