package dawsoncollege.android.pokemac

import android.content.Context
import dawsoncollege.android.pokemac.Database.*
import kotlin.math.floor
import kotlin.math.pow
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */

class Pokemon(
    var name: String,
    var species : String,
    var type1: String,
    var type2: String?,
    var baseReward :Int,
    var maxHp:Int,
    var atk: Int,
    var def: Int,
    var specialAtk: Int,
    var specialDef: Int,
    var speed: Int,
    val front: String,
    val back: String,
    var level: Int = 0,
    var move1: String,
    var move2: String,
    var move3: String,
    var move4: String,
    var experience: Int = 0,
    var hp: Int = 0,
) {

    /**
     * Set the stat depending on its level.
     */
    fun setStatValues() {
        this.maxHp = getMaxHp(this.maxHp, this.level)
        this.atk = getStat(this.atk, this.level)
        this.def = getStat(this.def, this.level)
        this.specialAtk = getStat(this.specialAtk, this.level)
        this.specialDef = getStat(this.specialAtk, this.level)
        this.speed = getStat(this.speed, this.level)
    }

    /**
     * Set the current hp to max Hp.
     */
    fun resetHp() {
        this.hp = maxHp
    }
}

/**
 * Converts the experience to a level.
 * @param experience the experience that the Pokemon has.
 * @return the level of the Pokemon.
 */
fun getLevel(experience: Int): Int {
    if (experience<0){
        throw java.lang.IllegalArgumentException("Experiences may not be negative")
    }
    return floor(Math.pow(experience.toDouble(), (1.0 / 3.0))).toInt()
}

/**
 * Get the maxHp of a Pokemon depending on its level.
 * @param hp the base hp of the Pokemon.
 * @param lvl the level of the Pokemon.
 * @return Int which represents the max Hp of the Pokemon.
 */
fun getMaxHp(hp: Int, lvl: Int): Int {
    return floor((((hp + 10) * lvl) / 50.0)).toInt() + lvl + 10
}

/**
 * Return a Pokemon object by converting a CollectionEntity.
 * @param collectionEntity the CollectionEntity to be converted to a Pokemon object.
 * @param context to have access to our database.
 * @return Pokemon object that has been converted from ColletionEntity to Pokemon.
 */
suspend fun getPokemonCollection(collectionEntity: CollectionEntity, context: Context) : Pokemon{
    val db = getDb(context)
    loadPokemonByName(collectionEntity.species, context)
    val data = db.PokemonDao().getPokemonByName(collectionEntity.species)!!

    val types = data.types.split(",")
    return if (types.size == 1){
        Pokemon(collectionEntity.name, collectionEntity.species, types[0].lowercase(), null, data.exp, data.maxHp,
            data.attack, data.defense, data.specialAtk, data.specialDef, data.speed, data.frontSprite, data.backSprite,
            collectionEntity.level, collectionEntity.move_1, collectionEntity.move_2, collectionEntity.move_3, collectionEntity.move_4,
            collectionEntity.current_Exp, collectionEntity.current_hp)

    } else{
        Pokemon(collectionEntity.name, collectionEntity.species, types[0].lowercase(), types[1].lowercase(), data.exp, data.maxHp,
            data.attack, data.defense, data.specialAtk, data.specialDef, data.speed, data.frontSprite, data.backSprite,
            collectionEntity.level, collectionEntity.move_1, collectionEntity.move_2, collectionEntity.move_3, collectionEntity.move_4,
            collectionEntity.current_Exp, collectionEntity.current_hp)
    }
}

/**
 * Set the starter Pokemon to the team.
 * @param pokemonName the name of the starter Pokemon.
 * @param context to have access to the database.
 */
suspend fun setPokemonStarter(pokemonName: String, context: Context){
    val db = getDb(context)

    loadPokemonByName(pokemonName, context)
    val pokemon = db.PokemonDao().getPokemonByName(pokemonName)!!


    // Get the 4 moves for the pokemon
    println("Getting $pokemonName 4 moves.")
    var moves = getPokemonMoveList(pokemonName, 5)
    while (moves == null){
        moves = getPokemonMoveList(pokemonName, 5)
    }
    val types = pokemon.types.split(",")
    val pokemonInfo = if (types.size == 1){
        Pokemon(pokemon.name, pokemon.name, types[0].lowercase(), null, pokemon.exp, pokemon.maxHp,
            pokemon.attack, pokemon.defense, pokemon.specialAtk, pokemon.specialDef, pokemon.speed, pokemon.frontSprite, pokemon.backSprite,
            5, moves[0], moves[1], moves[2], moves[3])

    } else{
        Pokemon(pokemon.name, pokemon.name, types[0].lowercase(), types[1].lowercase(), pokemon.exp, pokemon.maxHp,
            pokemon.attack, pokemon.defense, pokemon.specialAtk, pokemon.specialDef, pokemon.speed, pokemon.frontSprite, pokemon.backSprite,
            5, moves[0], moves[1], moves[2], moves[3])
    }
    // Set the stat to match the level
    pokemonInfo.setStatValues()
    pokemonInfo.resetHp()

    val movesPP = mutableListOf<Int>()

    for (move in moves){
        if (move == "null"){
            movesPP.add(0)
        }
        else{
            movesPP.add(getMovePP(move, context))
        }
    }

    db.CollectionDao().insertCollection(
        CollectionEntity(pokemonInfo.species, pokemonInfo.species, Math.pow(5.0, 3.0).toInt(),
            pokemonInfo.hp, 5, moves[0], moves[1], moves[2], moves[3], movesPP[0],
            movesPP[1], movesPP[2], movesPP[3], 1)
    )
    println("Pokemon added to the database.")

}

/**
 * Creates a random Pokemon from the Pokedex and returns it.
 * @param level level of the Pokemon to be created.
 * @param context to have access to the database.
 * @param position the position of where it will be save in the database.
 * @return Pokemon object to be returned after loading all its assets.
 */
suspend fun getRandomPokemonObject(level: Int, context: Context, position: Int): Pokemon{
    val db = getDb(context)
    loadPokedex(context)
    val pokedex = db.PokedexDao().getAll()
    println("pokedex loaded")

    val pokemonName = pokedex[Random.nextInt(pokedex.size)].name
    loadPokemonByName(pokemonName, context)
    val pokemon = db.PokemonDao().getPokemonByName(pokemonName)!!

    // Get the 4 moves for the pokemon
    println("Getting $pokemonName 4 moves.")
    var moves = getPokemonMoveList(pokemonName, level)
    while (moves == null){
        moves = getPokemonMoveList(pokemonName, level)
    }
    val types = pokemon.types.split(",")
    val pokemonInfo = if (types.size == 1){
        Pokemon(pokemon.name, pokemon.name, types[0].lowercase(), null, pokemon.exp, pokemon.maxHp,
            pokemon.attack, pokemon.defense, pokemon.specialAtk, pokemon.specialDef, pokemon.speed, pokemon.frontSprite, pokemon.backSprite,
            level, moves[0], moves[1], moves[2], moves[3])

    } else{
        Pokemon(pokemon.name, pokemon.name, types[0].lowercase(), types[1].lowercase(), pokemon.exp, pokemon.maxHp,
            pokemon.attack, pokemon.defense, pokemon.specialAtk, pokemon.specialDef, pokemon.speed, pokemon.frontSprite, pokemon.backSprite,
            level, moves[0], moves[1], moves[2], moves[3])
    }
    // Set the stat to match the level
    pokemonInfo.setStatValues()
    pokemonInfo.resetHp()

    val movesPP = mutableListOf<Int>()

    for (move in moves){
        if (move == "null"){
            movesPP.add(0)
        }
        else{
            movesPP.add(getMovePP(move, context))
        }
    }

    db.CollectionDao().insertCollection(
        CollectionEntity(pokemonInfo.species, pokemonInfo.species, Math.pow(level.toDouble(), 3.0).toInt(),
            pokemonInfo.hp, level, moves[0], moves[1], moves[2], moves[3], movesPP[0],
            movesPP[1], movesPP[2], movesPP[3], position)
    )
    println("Pokemon added to the database.")
    return pokemonInfo
}

/**
 * Randomly assigns a level to the enemy Pokemon.
 * @param context to have access to the database.
 * @return Int that is level of the enemy Pokemon.
 */
suspend fun setEnemyLevel(context: Context): Int{
    val db = getDb(context)
    val team = db.CollectionDao().getTeam()
    var max = 0
    var min = 1
    for (pokemon in team){
        val level = getLevel(pokemon.current_Exp)
        if (level < min) min = level
        if (level > max) max = level
    }
    if ((min - 5) <= 0) min = 1
    return Random.nextInt(min, max+5)
}

/**
 * Return the stat of the Pokemon depending on its level.
 * @param stat the stat to be changed then return.
 * @param lvl the level of the Pokemon.
 * @return Int that has been changed by level.
 */
private fun getStat(stat: Int, lvl: Int): Int {
    return floor((((stat + 10) * lvl) / 50.0)).toInt() + 10
}