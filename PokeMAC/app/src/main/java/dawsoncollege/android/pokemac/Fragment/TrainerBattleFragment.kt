package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentTrainerBattleBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.properties.Delegates
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class TrainerBattleFragment: Fragment(R.layout.fragment_trainer_battle) {

    // Fields
    private lateinit var binding: FragmentTrainerBattleBinding
    private lateinit var ally: Pokemon
    private lateinit var enemy: Pokemon
    private lateinit var db: PokemonDatabase
    private var userAlive: Boolean = true
    private var trainerAlive: Boolean = true
    private var enemyAlive: Int = 0
    private var trainer by Delegates.notNull<Int>()

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrainerBattleBinding.inflate(layoutInflater)
        return binding.root
    }


    /**
     * Bindings are done here and the function runs from here (default function).
     * Loads all the information for ally and enemy trainer.
     * If ally or enemy Pokemon has fainted then we search for the next one in the team.
     * If none is found then battle is lost or won.
     * If the ally has move then we get attack by the trainer.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        trainer = arguments?.getInt("trainer") ?: -1
        binding.menu.visibility = View.GONE
        binding.menuText.visibility = View.GONE
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireActivity().applicationContext)

            withContext(Dispatchers.Main){
                binding.loadingIndicator.visibility = View.VISIBLE
                binding.bottom.visibility = View.GONE
            }
            // Loading info
            userAlive = loadAlly()
            if (!userAlive){
                getNextAlly(requireContext().applicationContext)
                userAlive = loadAlly()
            }
            // Check level up and see if theres a new move.
            if (userAlive){
                withContext(Dispatchers.Main){
                    // Check to see if the pokemon leveled up or not
                    if (ally.level < getLevel(ally.experience)){
                        withContext(Dispatchers.IO){
                            val move = getPokemonNewMove(ally.species, getLevel(ally.experience))
                            if (!move.isNullOrEmpty()){
                                withContext(Dispatchers.Main){
                                    val bundle = Bundle()
                                    bundle.putString("battleType", "trainer")
                                    view.findNavController().navigate(R.id.action_trainerBattleFragment_to_chooseMoveFragment, bundle)
                                }
                            }
                        }
                    }
                }
            }
            trainerAlive = loadTrainer()
            // Setting up the pokeballs image (enemy left)
            val enemyTeam = db.CollectionDao().getEnemyTeam()
            for (pokemon in enemyTeam){
                if (pokemon.current_hp > 0){
                    enemyAlive++
                }
            }

            withContext(Dispatchers.Main){
                // If user or enemy has no enemy left. Go back to main menu
                if (!userAlive || !trainerAlive) {
                    bindingMenu()
                }
                // Continue to what you want to do.
                else{
                    // Load bindings.
                    bindingAlly()
                    bindingEnemy()
                    bindingPokeball()
                    // If player move (go to bag or swap)
                    if (arguments?.getBoolean("move")!!){
                        withContext(Dispatchers.IO){
                            val dmg = attackAlly()
                            println("Attack ally: $dmg")
                            withContext(Dispatchers.Main){
                                showDamageMessage(dmg)
                                if (!userAlive){
                                    bindingMenu()
                                }
                                else{
                                    bindingAlly()
                                    bindingEvent()
                                }
                            }
                        }
                    }
                    // If we did not move.
                    else{
                        bindingEvent()
                    }
                    withContext(Dispatchers.IO){
                        // Load the image
                        val front = getBitmap(enemy.front)
                        val back = getBitmap(ally.back)
                        withContext(Dispatchers.Main){
                            binding.pokemonImage.setImageBitmap(back)
                            binding.enemyImage.setImageBitmap(front)
                        }
                    }
                }
            }
        }
    }

    //------------------------------Fighting Functions---------------------------------------//

    /**
     * Damage report in Toast by enemy Pokemon.
     * @param dmg damage or healing done by enemy Pokemon.
     */
    private suspend fun showDamageMessage(dmg: Int){
        // Pokemon healed
        if (dmg > 0){
            val message = resources.getString(R.string.enemy_attack_heal, dmg)
            showToast(message)
        }
        // Pokemon miss attack
        else if (dmg == 0){
            val message = resources.getString(R.string.enemy_attack_miss)
            showToast(message)
        }
        // Pokemon died
        else if (dmg == -9999){
            val message = resources.getString(R.string.enemy_attack_faint)
            showToast(message)
            withContext(Dispatchers.IO){
                val currentAlly = db.CollectionDao().getPokemon(1)!!
                currentAlly.current_hp = 0
                db.CollectionDao().updateCollection(currentAlly)
                getNextAlly(requireContext().applicationContext)
                userAlive = loadAlly()
            }
        }
        // Pokemon attack
        else{
            val message = resources.getString(R.string.enemy_attack_damage, dmg)
            showToast(message)
        }
    }

    /**
     * Toast to show what the enemy Pokemon did as damage.
     * @param message the message to be shown in the Toast.
     */
    private fun showToast(message: String) =
        Toast.makeText(requireActivity().applicationContext, message, Toast.LENGTH_SHORT).show()

    /**
     * Attack the ally Pokemon
     * @return return the damage done by enemy Pokemon.
     */
    private suspend fun attackAlly(): Int {
        val enemyInfo = db.CollectionDao().getPokemon(-1)!!
        var attack = false
        var moveName = ""
        var moveNumber = 0
        while(!attack){
            moveNumber = Random.nextInt(4)
            when (moveNumber) {
                1 -> {
                    if (enemyInfo.pp_1 != 0){
                        moveName = enemy.move1
                        attack = true
                    }
                }
                2 -> {
                    if (enemyInfo.pp_2 != 0){
                        moveName = enemy.move2
                        attack = true
                    }
                }
                3 -> {
                    if (enemyInfo.pp_3 != 0){
                        moveName = enemy.move3
                        attack = true
                    }
                }
                else -> {
                    if (enemyInfo.pp_4 != 0){
                        moveName = enemy.move4
                        attack = true
                    }
                }
            }
        }
        loadMove(moveName, requireActivity().applicationContext)
        val move = db.MoveDao().getMove(moveName)!!
        val dmg = move.attack(ally, enemy, requireContext().applicationContext)
        savePokemon(moveNumber, dmg)
        return dmg
    }

    /**
     * Save the information about the attack done by the enemy.
     * @param moveNumber the move number of the enemy Pokemon.
     * @param dmg Healing or Damage done by the enemy Pokemon.
     */
    private suspend fun savePokemon(moveNumber: Int, dmg: Int){
        val enemyDb = db.CollectionDao().getPokemon(-1)!!
        val allyDb = db.CollectionDao().getPokemon(1)!!

        when (moveNumber) {
            1 -> {
                enemyDb.pp_1 -= 1
            }
            2 -> {
                enemyDb.pp_2 -= 1
            }
            3 -> {
                enemyDb.pp_3 -= 1
            }
            else -> {
                enemyDb.pp_4 -= 1
            }
        }
        // Pokemon healed
        if (dmg > 0){
            enemyDb.current_hp = enemy.hp
            db.CollectionDao().updateCollection(enemyDb)
        }
        if (dmg == -9999){
            allyDb.current_hp = 0
        }
        // Pokemon attack
        else if (dmg < 0){
            allyDb.current_hp = ally.hp
            db.CollectionDao().updateCollection(enemyDb)
            db.CollectionDao().updateCollection(allyDb)
        }
    }

    //-------------------------- Load Pokemons + helper functions----------------------------//

    /**
     * Load the enemy trainer or creates it.
     * @return Boolean to check if the enemy trainer has any Pokemon alive left.
     */
    private suspend fun loadTrainer(): Boolean{
        //Create the trainer
        if (trainer == -1){
            trainer = Random.nextInt(2,6)
            println(trainer)
            var position = -1
            var count = 0
            while (count != trainer){
                loadTrainerPokemon(position)
                position--
                count++
            }
            return loadEnemy(-1)
        }
        // Load the trainer
        else{
            return if (loadEnemy(-1)){
                true
            }
            // Swap and load
            else{
                getNextEnemy(requireContext().applicationContext)
                loadEnemy(-1)
            }
        }
    }

    /**
     * Creates all the Pokemons for the enemy trainer team is getting initialzing.
     * @param position the position of the Pokemon in the enemy trainer team.
     */
    private suspend fun loadTrainerPokemon(position: Int){
        val enemyDb = db.CollectionDao().getPokemon(position)
        // No enemy take a random one
        if (enemyDb == null) {
            enemy = getRandomPokemonObject(setEnemyLevel(requireContext().applicationContext),
                requireContext().applicationContext, position)
            // Set the stat to match the level
            enemy.setStatValues()
            enemy.resetHp()
        }
    }

    /**
     * Load the ally's pokemon at Position 1.
     * @return Boolean to check if the pokemon is alive or not.
     */
    private suspend fun loadAlly(): Boolean{
        val allyDb = db.CollectionDao().getPokemon(1)!!
        // Ally is dead
        if (allyDb.current_hp == 0){
            withContext(Dispatchers.Main){
                binding.menuText.text = resources.getString(R.string.battle_lost)
            }
            return false
        }
        // Load the pokemon
        else{
            ally = getPokemonCollection(allyDb, requireContext().applicationContext)

            // Set the stat to match the level
            ally.setStatValues()
            if (allyDb.current_hp > ally.maxHp){
                allyDb.current_hp = ally.maxHp
                db.CollectionDao().updateCollection(allyDb)
            }
            ally.hp = allyDb.current_hp
            return true
        }
    }

    /**
     * Load the enemy Pokemon at x position.
     * @return Boolean to check if the Pokemon is alive or not.
     */
    private suspend fun loadEnemy(position: Int): Boolean{
        val enemyDb = db.CollectionDao().getPokemon(position)
        // No enemy take a random one
        if (enemyDb == null){
            enemy = getRandomPokemonObject(setEnemyLevel(requireContext().applicationContext),
                requireContext().applicationContext, position)
            // Set the stat to match the level
            enemy.setStatValues()
            enemy.resetHp()
            return true
        }
        // Load the info needed from the enemy
        else{
            if (enemyDb.current_hp == 0){
                withContext(Dispatchers.Main){
                    binding.menuText.text = resources.getString(R.string.battle_won)
                }
                return false
            }
            else{
               enemy = getPokemonCollection(enemyDb, requireContext().applicationContext)
                enemy.setStatValues()
                if (enemyDb.current_hp > enemy.maxHp){
                    enemyDb.current_hp = enemy.maxHp
                    db.CollectionDao().updateCollection(enemyDb)
                }
                enemy.hp = enemyDb.current_hp
                return true
            }
        }
    }


    //-------------------------------- Bindings-------------------------------------//

    /**
     * Show the visibility of pokeballs depending on how many
     * Pokemons the enemy trainer has left.
     */
    private fun bindingPokeball(){
        if (enemyAlive == 1){
            binding.pokeball2.visibility = View.GONE
            binding.pokeball3.visibility = View.GONE
            binding.pokeball4.visibility = View.GONE
            binding.pokeball5.visibility = View.GONE
            binding.pokeball6.visibility = View.GONE
        }
        else if (enemyAlive == 2){
            binding.pokeball3.visibility = View.GONE
            binding.pokeball4.visibility = View.GONE
            binding.pokeball5.visibility = View.GONE
            binding.pokeball6.visibility = View.GONE
        }
        else if (enemyAlive == 3){
            binding.pokeball4.visibility = View.GONE
            binding.pokeball5.visibility = View.GONE
            binding.pokeball6.visibility = View.GONE
        }
        else if (enemyAlive == 4){
            binding.pokeball5.visibility = View.GONE
            binding.pokeball6.visibility = View.GONE
        }
        else if (enemyAlive == 5){
            binding.pokeball6.visibility = View.GONE
        }
    }

    /**
     * Set the binding for the allu Pokemon.
     */
    private fun bindingAlly(){
        binding.pokemonName.text = ally.name
        binding.pokemonLvl.text = ally.level.toString()
        binding.pokemonHp.max = ally.maxHp
        binding.pokemonHp.progress = ally.hp
        binding.pokemonHpText.text = resources.getString(
            R.string.hp_text, ally.hp,
            ally.maxHp)
    }

    /**
     * Set the binding for the enemy Pokemon.
     */
    private fun bindingEnemy(){
        binding.enemyName.text = enemy.name
        binding.enemyLvl.text = enemy.level.toString()
        binding.enemyHp.max = enemy.maxHp
        binding.enemyHp.progress = enemy.hp
        binding.enemyHpText.text = resources.getString(
            R.string.hp_text, enemy.hp,
            enemy.maxHp)
    }

    /**
     * Set the binding for the menu options when a battle is won or lost.
     */
    private fun bindingMenu(){
        binding.bottom.visibility = View.GONE
        binding.menu.visibility = View.VISIBLE
        binding.menuText.visibility = View.VISIBLE
        binding.top.visibility = View.INVISIBLE
        binding.loadingIndicator.visibility = View.GONE
        binding.menu.setOnClickListener {
            it.findNavController().navigate(R.id.action_trainerBattleFragment_to_menuFragment)
        }
    }

    /**
     * Set the action event to button event.
     * Lets the user choose what to do: Fighting, Using bag or Swapping.
     */
    private fun bindingEvent(){
        binding.loadingIndicator.visibility = View.GONE
        binding.bottom.visibility = View.VISIBLE
        binding.fight.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("trainer", trainer)
            bundle.putString("battleType", "trainer")
            it.findNavController().navigate(R.id.action_trainerBattleFragment_to_fightFragment, bundle)
        }
        binding.swap.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("trainer", trainer)
            bundle.putString("battleType", "trainer")
            it.findNavController().navigate(R.id.action_trainerBattleFragment_to_swapFragment, bundle)
        }
        binding.bag.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("trainer", trainer)
            bundle.putString("battleType", "trainer")
            it.findNavController().navigate(R.id.action_trainerBattleFragment_to_bagFragment, bundle)
        }
    }
}