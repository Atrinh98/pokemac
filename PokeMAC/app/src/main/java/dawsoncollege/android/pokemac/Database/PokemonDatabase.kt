package dawsoncollege.android.pokemac.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dawsoncollege.android.pokemac.PokedexDAO

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Database(entities = [CollectionEntity::class, MoveEntity::class,
    PokemonEntity::class, PokedexEntity::class, TypeEntity::class], version =1)
abstract class PokemonDatabase: RoomDatabase() {
    abstract fun CollectionDao(): CollectionDao
    abstract fun MoveDao(): MoveDao
    abstract fun PokemonDao(): PokemonDao
    abstract fun PokedexDao(): PokedexDAO
    abstract fun TypeDao(): TypeDao

}

// Pokemon Database
private var pokemonDatabase: PokemonDatabase? = null

/**
 * Singleton Database PokemonDatabase.
 * @param context to have access to the database.
 * @return the database.
 */
fun getDb(context: Context): PokemonDatabase {
    if (pokemonDatabase == null){
        pokemonDatabase = Room.databaseBuilder(
            context,
            PokemonDatabase::class.java,
            "PokeMAC-Database"
        ).build()
        return pokemonDatabase as PokemonDatabase
    }else{
        return pokemonDatabase as PokemonDatabase
    }
}