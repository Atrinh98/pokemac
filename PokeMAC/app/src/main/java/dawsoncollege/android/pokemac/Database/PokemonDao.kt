package dawsoncollege.android.pokemac.Database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Dao
interface PokemonDao {

    /**
     * Query to get Pokemon with the same id.
     * @param id the id of the Pokemon.
     * @return PokemonEntity if it exists or else null.
     */
    @Query("SELECT * FROM PokemonEntity WHERE id = :id")
    suspend fun getPokemonById(id: Int): PokemonEntity?

    /**
     * Query to get Pokemon with the same species.
     * @param species the species of the Pokemon.
     * @return PokemonEntity if it exists or else null.
     */
    @Query("Select * from PokemonEntity where name = :species")
    suspend fun getPokemonByName(species: String): PokemonEntity?

    /**
     * Insert query to insert PokemonEntity to the database.
     * @param pokemon PokemonEntity to insert in the database.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemon(vararg pokemon: PokemonEntity)
}