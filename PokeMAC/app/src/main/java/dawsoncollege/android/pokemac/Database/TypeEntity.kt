package dawsoncollege.android.pokemac.Database

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import dawsoncollege.android.pokemac.getTypeJSON
import org.json.JSONObject


/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Entity(primaryKeys = ["attackType", "defenseType"])
class TypeEntity (
    @ColumnInfo(name="attackType") val attackType: String,
    @ColumnInfo(name="defenseType") val defenseType: String,
    @ColumnInfo(name="damage") val damage: Double
    )

/**
 * Get the type multiplier from the types.
 * @param type1 move type.
 * @param type2 defender's type.
 * @param context to have access to the database.
 * @return type multiplier.
 */
suspend fun getTypeMultiplier(type1: String, type2: String, context: Context): Double{
    // Check to see if it is in the database.
    val db = getDb(context)
    val type = db.TypeDao().checkType(type1)
    // If not then we get the info from the API.
    if (type == null){

        println("$type1 getting type info from API.")
        var data = getTypeJSON(type1)
        while(data == null){
            data = getTypeJSON(type1)
        }
        // Insert all the move type info to the database.
        val typeInfo = JSONObject(data)
        val keys = typeInfo.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            val value = typeInfo[key]
            println(key is String)
            println(value is String)
            db.TypeDao().insertType(TypeEntity(type1, key, getMultiplier(value as String)))
            println("Add $type1 and $key relation to database")
        }
    }
    // If nothing is found then we return 1.0 as multiplier.
    if (db.TypeDao().getDamage(type1, type2) == null){
        return 1.0
    }
    // Return the multiplier from the database.
    else{
        return db.TypeDao().getDamage(type1, type2)!!
    }
}

/**
 * Converts the multiplier effect to a number (Double).
 * @param effect the multiplier effect.
 * @return the multiplier converted to Double.
 */
private fun getMultiplier(effect: String): Double{
    // For validation, if the effect is not found.
    var multi = 1.0
    when (effect){
        "not_very_effective" -> multi = 0.5
        "super_effective" -> multi = 2.0
        "no_effect" -> multi = 0.0
    }
    return multi
}