package dawsoncollege.android.pokemac

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dawsoncollege.android.pokemac.Database.CollectionEntity
import dawsoncollege.android.pokemac.databinding.PokemonRecyclerBinding

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */

/**
 * RecyclerView to show the collection of our Pokemons.
 */
class PokemonRecyclerViewAdapter(private val listPokemons: MutableList<CollectionEntity>,
                                 private val collectionId: TextView, private val positionId: TextView)
    : RecyclerView.Adapter<PokemonRecyclerViewAdapter.ViewHolder>() {
    class ViewHolder(val binding: PokemonRecyclerBinding) : RecyclerView.ViewHolder(binding.root)

    /**
     * Create a binding from scratch for the ViewHolder.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PokemonRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    /**
     * Do our binding to show what we want from the Pokemon.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = holder.binding
        val pokemon = listPokemons[position]

        binding.pokemonName.text = pokemon.name
        binding.pokemonLvl.text = "Level: ${getLevel(pokemon.current_Exp)}"
        binding.pokemonHp.text = "${pokemon.current_hp}Hp"
        binding.swapButton.setOnClickListener {
            collectionId.text = pokemon.collectionId.toString()
            positionId.text = pokemon.position.toString()
        }
    }

    /**
     * Get the item size of the list.
     */
    override fun getItemCount(): Int = listPokemons.size
}
