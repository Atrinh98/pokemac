package dawsoncollege.android.pokemac.Fragment

import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentChangeTeamBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChangeTeamFragment : Fragment(R.layout.fragment_change_team) {

    private lateinit var binding: FragmentChangeTeamBinding

    private lateinit var adapter: PokemonRecyclerViewAdapter

    private var click: CLICK = CLICK.NONE
    private lateinit var db: PokemonDatabase
    private lateinit var collection: MutableList<CollectionEntity>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangeTeamBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireActivity().applicationContext)
            collection = db.CollectionDao().getCollection()
            val team = db.CollectionDao().getTeam()
            withContext(Dispatchers.Main){
                bindingName(team)
                adapter = PokemonRecyclerViewAdapter(collection, binding.collectionId, binding.positionRecycler)
                binding.pokemonRecycler.adapter = adapter
                binding.pokemonRecycler.layoutManager = LinearLayoutManager(requireContext().applicationContext)
                bindingEvent()
            }
            loadBitmap()
        }
    }


    // Enum to keep track of which container has been clicked
    private enum class CLICK {ONE, TWO, THREE, FOUR, FIVE, SIX, NONE}

    private fun setName(position: Int, name: String){
        when (position){
            1 -> binding.name1.text = name
            2 -> binding.name2.text = name
            3 -> binding.name3.text = name
            4 -> binding.name4.text = name
            5 -> binding.name5.text = name
            6 -> binding.name6.text = name
        }
    }

    private suspend fun swapCollectionHelper(position: Int, collectionEnt: CollectionEntity){
        val pokemon = db.CollectionDao().getPokemon(position)
        // Swap
        if (pokemon != null){
            println("in swap")
            pokemon.position = 0
            db.CollectionDao().updateCollection(pokemon)
            collectionEnt.position = position
            db.CollectionDao().updateCollection(collectionEnt)
            collection = db.CollectionDao().getCollection()
            withContext(Dispatchers.Main){
                adapter.notifyItemChanged(binding.positionRecycler.text.toString().toInt())
                binding.positionRecycler.text = ""
                setName(position, collectionEnt.name)
            }
        }
        // Take place
        else{
            println(" in tale place")
            collectionEnt.position = position
            db.CollectionDao().updateCollection(collectionEnt)
            collection = db.CollectionDao().getCollection()
            withContext(Dispatchers.Main){
                adapter.notifyItemRemoved(binding.positionRecycler.text.toString().toInt())
                binding.positionRecycler.text = ""
                setName(position, collectionEnt.name)
            }
        }
        loadImage(position)
    }

    // Swap pokemon with collection
    private suspend fun swapCollection(){
        val id = Integer.parseInt(binding.collectionId.text.toString())
        val collection = db.CollectionDao().getPokemonWithId(id)
        when (click){
            CLICK.ONE -> swapCollectionHelper(1, collection)
            CLICK.TWO -> swapCollectionHelper(2, collection)
            CLICK.THREE -> swapCollectionHelper(3, collection)
            CLICK.FOUR -> swapCollectionHelper(4, collection)
            CLICK.FIVE -> swapCollectionHelper(5, collection)
            CLICK.SIX -> swapCollectionHelper(6, collection)
            else ->{}
        }
    }

    private fun setImageNull(position: Int){
        when (position){
            1 -> binding.image1.setImageDrawable(resources.getDrawable(R.drawable.question))
            2 -> binding.image2.setImageDrawable(resources.getDrawable(R.drawable.question))
            3 -> binding.image3.setImageDrawable(resources.getDrawable(R.drawable.question))
            4 -> binding.image4.setImageDrawable(resources.getDrawable(R.drawable.question))
            5 -> binding.image5.setImageDrawable(resources.getDrawable(R.drawable.question))
            6 -> binding.image6.setImageDrawable(resources.getDrawable(R.drawable.question))
        }
    }

    // Helper method
    private suspend fun swapPokemonHelper(pokemonPosition: Int, otherPosition: Int){
        val pokemon = db.CollectionDao().getPokemon(pokemonPosition)
        val other = db.CollectionDao().getPokemon(otherPosition)
        // Current null
        if (pokemon != null && other == null){
            pokemon.position = otherPosition
            db.CollectionDao().updateCollection(pokemon)
            withContext(Dispatchers.Main){
                setName(otherPosition, pokemon.name)
                setName(pokemonPosition, "")
                setImageNull(pokemonPosition)
            }
            println("other to pokemon")
        }
        // Other is null
        else if (pokemon == null && other != null){
            other.position = pokemonPosition
            db.CollectionDao().updateCollection(other)
            withContext(Dispatchers.Main){
                setName(pokemonPosition, other.name)
                setName(otherPosition, "")
                setImageNull(otherPosition)
            }
            println("pokemon to other")
        }
        // Swap
        else if(pokemon != null && other != null) {
            pokemon.position = otherPosition
            other.position = pokemonPosition
            db.CollectionDao().updateCollection(other)
            db.CollectionDao().updateCollection(pokemon)
            withContext(Dispatchers.Main){
                setName(otherPosition, pokemon.name)
                setName(pokemonPosition, other.name)
            }
            println("both swap")
        }
    }

    private suspend fun loadImage(position: Int){
        val species = db.CollectionDao().getPokemon(position)
        if (species != null){
            val name = species.species
            loadPokemonByName(name, requireContext().applicationContext)
            val frontData = db.PokemonDao().getPokemonByName(name)!!.frontSprite
            val front = getBitmap(frontData)
            withContext(Dispatchers.Main){
                when (position) {
                    1 -> binding.image1.setImageBitmap(front)
                    2 -> binding.image2.setImageBitmap(front)
                    3 -> binding.image3.setImageBitmap(front)
                    4 -> binding.image4.setImageBitmap(front)
                    5 -> binding.image5.setImageBitmap(front)
                    6 -> binding.image6.setImageBitmap(front)
                }
            }
        }
    }

    // Swap the Pokemon with the current team
    private suspend fun swapPokemon(clickOther: CLICK){
        when (click) {
            CLICK.ONE -> {
                when (clickOther) {
                    CLICK.TWO -> {
                        swapPokemonHelper(1,2)
                        loadImage(1)
                        loadImage(2)
                    }
                    CLICK.THREE -> {
                        swapPokemonHelper(1,3)
                        loadImage(1)
                        loadImage(3)
                    }
                    CLICK.FOUR -> {
                        swapPokemonHelper(1,4)
                        loadImage(1)
                        loadImage(4)
                    }
                    CLICK.FIVE -> {
                        swapPokemonHelper(1,5)
                        loadImage(1)
                        loadImage(5)
                    }
                    CLICK.SIX -> {
                        swapPokemonHelper(1,6)
                        loadImage(1)
                        loadImage(6)
                    }
                    else -> {}
                }
            }
            CLICK.TWO -> {
                when (clickOther) {
                    CLICK.ONE -> {
                        swapPokemonHelper(2,1)
                        loadImage(2)
                        loadImage(1)
                    }
                    CLICK.THREE -> {
                        swapPokemonHelper(2,3)
                        loadImage(2)
                        loadImage(3)
                    }
                    CLICK.FOUR -> {
                        swapPokemonHelper(2,4)
                        loadImage(2)
                        loadImage(4)
                    }
                    CLICK.FIVE -> {
                        swapPokemonHelper(2,5)
                        loadImage(2)
                        loadImage(5)
                    }
                    CLICK.SIX -> {
                        swapPokemonHelper(2,6)
                        loadImage(2)
                        loadImage(6)
                    }
                    else -> {}
                }
            }
            CLICK.THREE -> {
                when (clickOther) {
                    CLICK.ONE -> {
                        swapPokemonHelper(3,1)
                        loadImage(3)
                        loadImage(1)
                    }
                    CLICK.TWO -> {
                        swapPokemonHelper(3,2)
                        loadImage(3)
                        loadImage(2)
                    }
                    CLICK.FOUR -> {
                        swapPokemonHelper(3,4)
                        loadImage(3)
                        loadImage(4)
                    }
                    CLICK.FIVE -> {
                        swapPokemonHelper(3,5)
                        loadImage(3)
                        loadImage(5)
                    }
                    CLICK.SIX -> {
                        swapPokemonHelper(3,6)
                        loadImage(3)
                        loadImage(6)
                    }
                    else -> {}
                }
            }
            CLICK.FOUR -> {
                when (clickOther) {
                    CLICK.ONE -> {
                        swapPokemonHelper(4,1)
                        loadImage(4)
                        loadImage(1)
                    }
                    CLICK.TWO -> {
                        swapPokemonHelper(4,2)
                        loadImage(4)
                        loadImage(2)
                    }
                    CLICK.THREE -> {
                        swapPokemonHelper(4,3)
                        loadImage(4)
                        loadImage(3)
                    }
                    CLICK.FIVE -> {
                        swapPokemonHelper(4,5)
                        loadImage(4)
                        loadImage(5)
                    }
                    CLICK.SIX -> {
                        swapPokemonHelper(4,6)
                        loadImage(4)
                        loadImage(6)
                    }
                    else -> {}
                }
            }
            CLICK.FIVE -> {
                when (clickOther) {
                    CLICK.ONE -> {
                        swapPokemonHelper(5,1)
                        loadImage(5)
                        loadImage(1)
                    }
                    CLICK.TWO -> {
                        swapPokemonHelper(5,2)
                        loadImage(5)
                        loadImage(2)
                    }
                    CLICK.THREE -> {
                        swapPokemonHelper(5,3)
                        loadImage(5)
                        loadImage(3)
                    }
                    CLICK.FOUR -> {
                        swapPokemonHelper(5,4)
                        loadImage(5)
                        loadImage(4)
                    }
                    CLICK.SIX -> {
                        swapPokemonHelper(5,6)
                        loadImage(5)
                        loadImage(6)
                    }
                    else -> {}
                }
            }
            CLICK.SIX -> {
                when (clickOther) {
                    CLICK.ONE -> {
                        swapPokemonHelper(6,1)
                        loadImage(6)
                        loadImage(1)
                    }
                    CLICK.TWO -> {
                        swapPokemonHelper(6,2)
                        loadImage(6)
                        loadImage(2)
                    }
                    CLICK.THREE -> {
                        swapPokemonHelper(6,3)
                        loadImage(6)
                        loadImage(3)
                    }
                    CLICK.FOUR -> {
                        swapPokemonHelper(6,4)
                        loadImage(6)
                        loadImage(4)
                    }
                    CLICK.FIVE -> {
                        swapPokemonHelper(6,5)
                        loadImage(6)
                        loadImage(5)
                    }
                    else -> {}
                }
            }
            else -> {}
        }
    }


    // Load the images for all the Pokemons
    private suspend fun loadBitmap(){
        loadImage(1)
        loadImage(2)
        loadImage(3)
        loadImage(4)
        loadImage(5)
        loadImage(6)
    }


    //------------------Binding-------------------------//
    // Set the name of the pokemon
    private fun bindingName(list :MutableList<CollectionEntity>){
        for (pokemon in list){
            println("${pokemon.position} :${pokemon.name}")
            setName(pokemon.position, pokemon.name)
        }
    }


    private fun containerSelect(position: Int){
        when (position){
            1 -> binding.container1.setBackgroundColor(resources.getColor(R.color.select_swap))
            2 -> binding.container2.setBackgroundColor(resources.getColor(R.color.select_swap))
            3 -> binding.container3.setBackgroundColor(resources.getColor(R.color.select_swap))
            4 -> binding.container4.setBackgroundColor(resources.getColor(R.color.select_swap))
            5 -> binding.container5.setBackgroundColor(resources.getColor(R.color.select_swap))
            6 -> binding.container6.setBackgroundColor(resources.getColor(R.color.select_swap))
        }
    }


    private fun bindingEventHelper(clickNumber: CLICK, position: Int){
        // Swap with a collection
        if (click == CLICK.NONE && binding.collectionId.text != ""){
            cancelAll()
            lifecycleScope.launch(Dispatchers.IO){
                click = clickNumber
                swapCollection()
                withContext(Dispatchers.Main){
                    binding.collectionId.text = ""
                    click = CLICK.NONE
                    cancelAll()
                }
            }
        }
        // Swap with team
        else if (click != clickNumber && click != CLICK.NONE && binding.collectionId.text == ""){
            cancelAll()
            lifecycleScope.launch(Dispatchers.IO){
                swapPokemon(clickNumber)
                click = CLICK.NONE
                withContext(Dispatchers.Main){
                    cancelAll()
                }
            }
        }
        // Highlight Pokemon #1
        else if (click != clickNumber && click == CLICK.NONE && binding.collectionId.text == "") {
            cancelAll()
            click = clickNumber
            containerSelect(position)
        }
        // Cancel the click
        else if (click == clickNumber){
            click = CLICK.NONE
            cancelAll()
        }
    }

    private suspend fun teamToCollectionHelper(position: Int, size: Int){
        val pokemon = db.CollectionDao().getPokemon(position)
        if (pokemon != null){
            pokemon.position = 0
            db.CollectionDao().updateCollection(pokemon)
            collection = db.CollectionDao().getCollection()
            withContext(Dispatchers.Main){
                setName(position, "")
                setImageNull(position)
                adapter.notifyItemInserted(size)
            }
        }
        click = CLICK.NONE
    }

    private suspend fun teamToCollection(){
        val size = db.CollectionDao().getCollection().size
        when(click){
            CLICK.ONE -> {
                teamToCollectionHelper(1, size)
            }
            CLICK.TWO -> {
                teamToCollectionHelper(2, size)
            }
            CLICK.THREE -> {
                teamToCollectionHelper(3, size)
            }
            CLICK.FOUR -> {
                teamToCollectionHelper(4, size)
            }
            CLICK.FIVE -> {
                teamToCollectionHelper(5, size)
            }
            CLICK.SIX -> {
                teamToCollectionHelper(6, size)
            }
            else ->{}
        }
    }

    private fun cancelAll(){
        binding.container1.setBackgroundColor(resources.getColor(R.color.swap01))
        binding.container2.setBackgroundColor(resources.getColor(R.color.swap02))
        binding.container3.setBackgroundColor(resources.getColor(R.color.swap03))
        binding.container4.setBackgroundColor(resources.getColor(R.color.swap04))
        binding.container5.setBackgroundColor(resources.getColor(R.color.swap05))
        binding.container6.setBackgroundColor(resources.getColor(R.color.swap06))
    }

    //Set the binding event for the buttons
    private fun bindingEvent(){
        binding.container1.setOnClickListener{
            bindingEventHelper(CLICK.ONE,1)
        }
        binding.container2.setOnClickListener {
            bindingEventHelper(CLICK.TWO, 2)
        }
        binding.container3.setOnClickListener {
            bindingEventHelper(CLICK.THREE, 3)
        }
        binding.container4.setOnClickListener {
            bindingEventHelper(CLICK.FOUR, 4)
        }
        binding.container5.setOnClickListener {
            bindingEventHelper(CLICK.FIVE, 5)
        }
        binding.container6.setOnClickListener {
            bindingEventHelper(CLICK.SIX, 6)
        }
        binding.backButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_changeTeamFragment_to_menuFragment)
        }
        binding.collection.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                teamToCollection()
            }
        }
    }


}

