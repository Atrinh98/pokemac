package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.Database.getDb
import dawsoncollege.android.pokemac.Database.getTypeMultiplier
import dawsoncollege.android.pokemac.R
import dawsoncollege.android.pokemac.databinding.FragmentMenuBinding
import dawsoncollege.android.pokemac.getLevel
import dawsoncollege.android.pokemac.getPokemonNewMove
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class MenuFragment : Fragment(R.layout.fragment_menu) {

    // Field
    private lateinit var binding: FragmentMenuBinding

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Lets the user decide where to go in the game.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setButtonEvent()
    }

    /**
     * Set the action binding for the buttons.
     * User decides which fragments or action he wants to go to.
     */
    private fun setButtonEvent(){
        binding.battleBtn.setOnClickListener {
            it.findNavController().navigate(R.id.action_menuFragment_to_chooseBattleFragment)
        }
        binding.pokeCenterBtn.setOnClickListener {
            it.findNavController().navigate(R.id.action_menuFragment_to_pokeCenterFragment)
        }
        binding.changeBtn.setOnClickListener {
            it.findNavController().navigate(R.id.action_menuFragment_to_changeTeamFragment)
        }
        binding.saveBtn.setOnClickListener {
            Toast.makeText(requireContext().applicationContext, "AutoSave only\nSave function not available.", Toast.LENGTH_SHORT).show()
        }
    }
}