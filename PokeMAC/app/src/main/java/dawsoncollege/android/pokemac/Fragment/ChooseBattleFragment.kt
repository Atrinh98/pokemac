package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentChooseBattleBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class ChooseBattleFragment : Fragment(R.layout.fragment_choose_battle) {

    // Fields
    private lateinit var binding: FragmentChooseBattleBinding
    private lateinit var db: PokemonDatabase

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChooseBattleBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * User decides if he wants to do wild battle (Pokemon) or trainer.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setButtonEvent()
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireActivity().applicationContext)

            // Delete all the enemy Pokemons from the database.
            deleteEnemy()

            // Check to see if we have Pokemon in our team and if we have a Pokemon at first position.
            val team = db.CollectionDao().getTeam()
            val first = db.CollectionDao().getPokemon(1)
            if (team.size != 0 && first == null){
                replaceFirst(requireContext().applicationContext)
            }
            // Set the event for the buttons.
            withContext(Dispatchers.Main){
                setButtonEvent()
            }
        }
    }

    /**
     * Delete all the enemy Pokemon to have new enemies everytime.
     */
    private suspend fun deleteEnemy(){
        val enemyTeam = db.CollectionDao().getEnemyTeam()
        for (enemy in enemyTeam){
            db.CollectionDao().deletePokemon(enemy.collectionId)
        }
    }

    /**
     * Sets the event to go to the desired battle fragment.
     */
    private fun setButtonEvent(){
        binding.pokemonBtn.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("move", false)
            it.findNavController().navigate(R.id.action_chooseBattleFragment_to_pokemonBattleFragment, bundle)
        }
        binding.trainerBtn.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("trainer", -1)
            bundle.putBoolean("move", false)
            it.findNavController().navigate(R.id.action_chooseBattleFragment_to_trainerBattleFragment, bundle)
        }
    }
}