package dawsoncollege.android.pokemac

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dawsoncollege.android.pokemac.databinding.ActivityMainBinding

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */

class MainActivity: AppCompatActivity() {

    // Fields
    private lateinit var binding: ActivityMainBinding

    /**
     * Create the binding for the MainActivity and return the root (default function).
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}