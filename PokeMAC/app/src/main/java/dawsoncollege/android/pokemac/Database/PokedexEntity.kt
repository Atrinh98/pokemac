package dawsoncollege.android.pokemac.Database

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import dawsoncollege.android.pokemac.getPokedexJSON

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Entity
data class PokedexEntity(
    @PrimaryKey val index: Int,
    @ColumnInfo(name = "name") val name: String
)

/**
 * Loads the Pokedex database.
 * @param context to have access to our database.
 */
suspend fun loadPokedex(context: Context){
        // Load the Pokedex
        val db = getDb(context)
        val pokedex = db.PokedexDao().getAll()

        // If no data in the Pokedex
        if (pokedex.isEmpty()){
            println("Filling up Pokedex with fetch API.")
            var data = getPokedexJSON()
            while (data == null){
                data = getPokedexJSON()
            }
            db.PokedexDao().insertAll(data)

            println("Pokedex Updated (all pokemon entries added to the database).")
        }
}