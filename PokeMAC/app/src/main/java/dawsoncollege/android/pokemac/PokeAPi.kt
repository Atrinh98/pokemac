package dawsoncollege.android.pokemac

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import dawsoncollege.android.pokemac.Database.PokedexEntity
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.net.URL
import javax.net.ssl.HttpsURLConnection
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */


private const val LOG_TAG = "POKEAPI"
const val POKE_API_BASE_URL = "https://pokeapi.co/api/v2"
const val POKEDEX_BASE_URL = "$POKE_API_BASE_URL/pokedex"
const val POKEMON_BASE_URL = "$POKE_API_BASE_URL/pokemon"
const val POKEMON_TYPE_URL = "$POKE_API_BASE_URL/type"
private val GSON: Gson = GsonBuilder().setPrettyPrinting().create()

/**
 * Return all the possible moves for a Pokemon at a certain level and below.
 * @param apiResponse the Json String.
 * @param level the level of the Pokemon.
 * @return MutableList of MoveQuery which contains the move name and level acquired.
 */
private fun simplifyPokemonMove(apiResponse: String, level: Int): MutableList<MoveQuery>{
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)

    val moves = mutableListOf<MoveQuery>()

    json["moves"].asJsonArray.map {
        val name = it.asJsonObject["move"].asJsonObject["name"].asString
        val lvl = it.asJsonObject["version_group_details"].asJsonArray[0].asJsonObject["level_learned_at"].asInt

        // Add only move that are under the level
        if (lvl <= level){
            moves.add(MoveQuery(name, lvl))
        }
    }
    return moves
}

/**
 * Return the move for a Pokemon at a certain level.
 * @param apiResponse the Json String.
 * @param level the level of the Pokemon.
 * @return Return the name of the move.
 */
private fun simplifyPokemonNewMove(apiResponse: String, level: Int): String?{
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)


    json["moves"].asJsonArray.map {
        val name = it.asJsonObject["move"].asJsonObject["name"].asString
        val lvl = it.asJsonObject["version_group_details"].asJsonArray[0].asJsonObject["level_learned_at"].asInt

        // Add only move that are under the level
        if (lvl == level){
            return name
        }
    }
    return null
}

/**
 * Return the move a Pokemon learned at x level.
 * @param pokemonName the name of the Pokemon.
 * @param level the level of the Pokemon.
 * @return Name of the move if it exists or else null.
 */
fun getPokemonNewMove(pokemonName: String, level: Int): String? {
    var move: String? = ""
    try{
        val conn = getConnection("${POKEMON_BASE_URL}/$pokemonName")
        if (conn != null){
            val inStream = conn.inputStream
            val response = inStream.bufferedReader().use { it.readText() }

            val data = simplifyPokemonNewMove(response, level)

            if (data == null){
                move = null
            }
            else{
                move = data
            }
            println("move api for new move: $move")
            inStream.close()
        }
        conn?.disconnect()
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return move
}

/**
 * Return all the possible moves for a Pokemon at a certain level and below.
 * @param pokemonName the name of the Pokemon.
 * @param level the level of the Pokemon.
 * @return MutableList of String containing all the move names.
 */
fun getPokemonMoveList(pokemonName: String, level: Int): MutableList<String>? {
    val moves = mutableListOf<String>()
    try{
        val conn = getConnection("${POKEMON_BASE_URL}/$pokemonName")
        if (conn != null){
            val inStream = conn.inputStream
            val response = inStream.bufferedReader().use { it.readText() }

            val data = simplifyPokemonMove(response, level)
            println("Moves available: ${data.size}")
            val index = mutableListOf<Int>()

            while (moves.size < 4){
                val random = Random.nextInt(data.size)
                if (!index.contains(random)){
                    index.add(random)
                    moves.add(data[random].name)
                    println(data[random].name)
                    println("Moves size: ${moves.size}")
                    if (moves.size == data.size){
                        moves.add("null")
                    }
                }
            }

            inStream.close()
        }
        conn?.disconnect()
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return moves
}

/**
 * Returns the move as a Json string.
 * @param apiResponse response from the fetch.
 * @return Json string of the move.
 */
fun simplifyMove(apiResponse: String): String {
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)
    if (json["power"].isJsonNull && json["accuracy"].isJsonNull) {
        val simplified = JsonObject().apply {
            addProperty(
                "name", json["name"].asString
            )
            addProperty(
                "accuracy", 0
            )
            addProperty(
                "max_PP", json["pp"].asInt
            )
            addProperty(
                "power", 0
            )
            addProperty(
                "heal", json["meta"].asJsonObject["healing"].asInt
            )
            addProperty(
                "damage_class", json["damage_class"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "type", json["type"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "target", json["target"].asJsonObject["name"].asString.uppercase()
            )
        }
        return GSON.toJson(simplified)
    }
    else if (json["accuracy"].isJsonNull) {
        val simplified = JsonObject().apply {
            addProperty(
                "name", json["name"].asString
            )
            addProperty(
                "accuracy", 0
            )
            addProperty(
                "max_PP", json["pp"].asInt
            )
            addProperty(
                "power", json["pp"].asInt
            )
            addProperty(
                "heal", json["meta"].asJsonObject["healing"].asInt
            )
            addProperty(
                "damage_class", json["damage_class"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "type", json["type"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "target", json["target"].asJsonObject["name"].asString.uppercase()
            )
        }
        return GSON.toJson(simplified)
    }
    else if (json["power"].isJsonNull){
        val simplified = JsonObject().apply {
            addProperty(
                "name", json["name"].asString
            )
            addProperty(
                "accuracy", json["accuracy"].asInt
            )
            addProperty(
                "max_PP", json["pp"].asInt
            )
            addProperty(
                "power", 0
            )
            addProperty(
                "heal", json["meta"].asJsonObject["healing"].asInt
            )
            addProperty(
                "damage_class", json["damage_class"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "type", json["type"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "target", json["target"].asJsonObject["name"].asString.uppercase()
            )
        }
        return GSON.toJson(simplified)
    }
    else {
        val simplified = JsonObject().apply {
            addProperty(
                "name", json["name"].asString
            )
            addProperty(
                "accuracy", json["accuracy"].asInt
            )
            addProperty(
                "max_PP", json["pp"].asInt
            )
            addProperty(
                "power", json["power"].asInt
            )
            addProperty(
                "heal", json["meta"].asJsonObject["healing"].asInt
            )
            addProperty(
                "damage_class", json["damage_class"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "type", json["type"].asJsonObject["name"].asString.uppercase()
            )
            addProperty(
                "target", json["target"].asJsonObject["name"].asString.uppercase()
            )
        }
        return GSON.toJson(simplified)
    }
}


/**
 * Get the move as a Json object.
 * @param moveName the name of the move.
 * @return JsonObject that contains the move information.
 */
fun getMoveJSON(moveName: String): JSONObject? {
    var data: String = ""
    try{
        val conn = getConnection("${POKE_API_BASE_URL}/move/$moveName")
        if (conn != null){
            val inStream = conn.inputStream
            val response = inStream.bufferedReader().use { it.readText() }

            data = simplifyMove(response)
            inStream.close()
        }
        conn?.disconnect()
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return JSONObject(data)
}

/**
 * Simplifies the API response from the pokedex endpoint.
 * The simplified JSON has the following format :
 * ```
 * [
 *   {
 *     "number" : 1,
 *     "name" : "bulbasaur"
 *   },
 *   {
 *     "number" : 2,
 *     "name" : "ivysaur"
 *   },
 *   ...
 *   {
 *     "number" : 151,
 *     "name" : "mew"
 *   }
 * ]
 * ```
 *
 * @param apiResponse the API response as received from PokeAPI's pokedex endpoint
 *                    (https://pokeapi.co/api/v2/pokedex)
 *
 * @return the simplified JSON as a String
 */
fun simplifyPokedexEntries(apiResponse: String): String {
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)

    val simplified = json["pokemon_entries"].asJsonArray.map {
        JsonObject().apply {
            addProperty(
                "number",
                it.asJsonObject["entry_number"].asInt
            )
            addProperty(
                "name",
                it.asJsonObject["pokemon_species"].asJsonObject["name"].asString
            )
        }
    }
    return GSON.toJson(simplified)
}

/**
 * Return the list of every Pokedex entry.
 * @return MutableList of every Pokedex Entry as PokedexEntity.
 */
fun getPokedexJSON(): MutableList<PokedexEntity>? {
    var data : String = ""
    try{
        val conn = getConnection("$POKEDEX_BASE_URL/2")
        if (conn != null){
            val inStream = conn.inputStream
            val response = inStream.bufferedReader().use{it.readText()}

            data = simplifyPokedexEntries(response)
            inStream.close()
        }
        conn?.disconnect()
    } catch (ioException: IOException){
        ioException.printStackTrace()
        return null
    }

    val jsonData = JSONArray(data)
    val pokedex = mutableListOf<PokedexEntity>()


    for (i in 0 until jsonData.length()){
        val jsonEntry: JSONObject = jsonData.getJSONObject(i)
        pokedex.add(PokedexEntity(jsonEntry.getInt("number"), jsonEntry.getString("name")))
    }
    return pokedex
}

/**
 * Simplifies the API response from the pokemon endpoint.
 * The simplified JSON has the following format :
 * ```
 * {
 *   "name" : "bulbasaur",
 *   "base_exp_reward" : 64,
 *   "types" : [
 *     "grass", "poison"
 *   ],
 *   "base_maxHp" : 45,
 *   "base_attack" : 49,
 *   "base_defense" : 49,
 *   "base_special-attack" : 65,
 *   "base_special-defense" : 65,
 *   "base_speed" : 45,
 *   "back_sprite" : "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-i/red-blue/transparent/back/1.png"
 *   "front_sprite" : "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-i/red-blue/transparent/1.png"
 * }
 * ```
 *
 * @param apiResponse the API response as received from PokeAPI's pokemon endpoint
 *                    (https://pokeapi.co/api/v2/pokemon)
 *
 * @return the simplified JSON as a String
 */
private fun simplifyPokemon(apiResponse: String): String {
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)

    val simplified = JsonObject().apply {
        addProperty(
            "name",
            json["name"].asString
        )
        addProperty(
            "id", json["id"].asInt
        )
        addProperty(
            "base_exp_reward",
            json["base_experience"].asInt
        )
        add(
            "types",
            JsonArray().apply {
                json["types"].asJsonArray.map {
                    it.asJsonObject["type"].asJsonObject["name"].asString
                }.forEach { this.add(it) }
            }
        )
        json["stats"].asJsonArray.associate {
            it.asJsonObject.run {
                (this["stat"].asJsonObject["name"].asString) to (this["base_stat"].asInt)
            }
        }.forEach {
            val statName = if (it.key == "hp") "maxHp" else it.key
            this.addProperty("base_$statName", it.value)
        }
        addProperty(
            "back_sprite",
            json["sprites"].asJsonObject["versions"].asJsonObject["generation-i"]
                .asJsonObject["red-blue"].asJsonObject["back_transparent"].asString
        )
        addProperty(
            "front_sprite",
            json["sprites"].asJsonObject["versions"].asJsonObject["generation-i"]
                .asJsonObject["red-blue"].asJsonObject["front_transparent"].asString
        )
    }

    return GSON.toJson(simplified)
}

/**
 * Return the Pokemon as a Json Object.
 * @param pokemonName the name of the Pokemon.
 * @return Json objecet represing the Pokemon if it exists or else null.
 */
fun getPokemonJSON(pokemonName: String): JSONObject? {
    var data : String = ""
    try{
        val conn = getConnection("$POKEMON_BASE_URL/$pokemonName")
        if (conn != null){
            val inStream = conn.inputStream
            val response = inStream.bufferedReader().use{it.readText()}

            data = simplifyPokemon(response)
            inStream.close()
        }
        conn?.disconnect()
    } catch (ioException: IOException){
        ioException.printStackTrace()
        return null
    }
    return JSONObject(data)
}

/**
 * Return the Bitmap object of the Pokemon front or back.
 * @param url the url to get the Bitmap.
 * @return Bitmap of the Pokemon front or back if it exists or else null.
 */
fun getBitmap(url: String): Bitmap?{
    var data : Bitmap? = null
    try{
        val conn = getConnection(url)
        if (conn != null){
            val inStream = conn.inputStream
            data = BitmapFactory.decodeStream(inStream)
            inStream.close()
        }
        conn?.disconnect()
    } catch (ioException: IOException){
        ioException.printStackTrace()
        return null
    }
    return data as Bitmap
}

/**
 * Get all the information of a type.
 * @param apiResponse the response from the fetch API.
 * @return Returns a Json String containing all the information of the move.
 */
private fun simplifyType(apiResponse: String): String{
    val json = GSON.fromJson(apiResponse, JsonObject::class.java)

    val firstGeneration = "generation-i"

    val damageRelations = json["damage_relations"].asJsonObject
//        if (json["generation"].asJsonObject["name"].asString == firstGeneration) {
//            json["damage_relations"].asJsonObject
//        } else {
//            json["past_damage_relations"].asJsonArray.first {
//                it.asJsonObject["generation"].asJsonObject["name"].asString == firstGeneration
//            }.asJsonObject
//        }

    val map = mutableMapOf<String, String>()

    damageRelations["double_damage_to"].asJsonArray.map { it.asJsonObject["name"].asString }
        .forEach { typeName -> map[typeName] = "super_effective" }
    damageRelations["half_damage_to"].asJsonArray.map { it.asJsonObject["name"].asString }
        .forEach { typeName -> map[typeName] = "not_very_effective" }
    damageRelations["no_damage_to"].asJsonArray.map { it.asJsonObject["name"].asString }
        .forEach { typeName -> map[typeName] = "no_effect" }

    return GSON.toJson(map)
}

/**
 * Returns the information of the type as Json object.
 * @param typeName the name of the type.
 * @return Json String of the type if it exists or else null.
 */
fun getTypeJSON(typeName: String): String?{
    var data = ""

    try{
        val conn = getConnection("https://pokeapi.co/api/v2/type/$typeName")
        if (conn != null){
            val inStream = conn.inputStream
            val response = inStream.bufferedReader().use{it.readText()}
            data = simplifyType(response)
            inStream.close()
        }
        conn?.disconnect()
    }
    catch (ioException: IOException){
        ioException.printStackTrace()
        return null
    }
    return data
}


/**
 * Get a connection with the Poke API.
 * @param url to connect and fetch from the API.
 * @return Https connection of the APi.
 */
private fun getConnection(url: String): HttpsURLConnection?{
    try{
        val newURL = URL(url)
        val conn = newURL.openConnection() as HttpsURLConnection

        conn.requestMethod = "GET"
        conn.connect()
        if (conn.responseCode == HttpsURLConnection.HTTP_OK){
            return conn
        }
    }catch (ioException: IOException){
        ioException.printStackTrace()
        return null
    }
    // If it reaches here means it is not ok
    return null
}



// Not too sure about the code below (Cuneyt part)

//fun getPokemonTypeDetailJson(id:Int): JsonObject? {
//    if(id.toString().toInt() != id){
//        throw IllegalArgumentException("Id must be integer ")
//    }
//    var conn =  getConnection("$POKEMON_TYPE_URL/$id")
//    var pokemonType= JsonObject()
//
//    try {
//        if (conn != null){
//            val inStream = conn.inputStream
//            val response = inStream.bufferedReader().use{it.readText()}
//            inStream.close()
//            pokemonType = simplfyPokemonType(response)
//        }
//    }catch (exc:IOException){
//        exc.printStackTrace()
//        return null
//    }
//    return pokemonType
//}
//
//
//
//fun simplfyPokemonType(apiResponse: String):JsonObject{
//    val json = GSON.fromJson(apiResponse, JsonObject::class.java)
//
//    val simplified = simplifyTypeRelations(apiResponse)
//    return GSON.fromJson(simplified,JsonObject::class.java)
//
//}
