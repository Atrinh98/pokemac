package dawsoncollege.android.pokemac.Database

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import dawsoncollege.android.pokemac.getPokemonJSON
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Entity
data class PokemonEntity (
    @PrimaryKey val id: Int,
    @ColumnInfo(name="name") val name: String,
    @ColumnInfo(name="types") val types: String,
    @ColumnInfo(name="max_hp") val maxHp: Int,
    @ColumnInfo(name="attack") val attack: Int,
    @ColumnInfo(name="defense") val defense: Int,
    @ColumnInfo(name="special_attack") val specialAtk: Int,
    @ColumnInfo(name="special_defense") val specialDef: Int,
    @ColumnInfo(name="speed") val speed: Int,
    @ColumnInfo(name="experience") val exp: Int,
    @ColumnInfo(name="back_sprite") val backSprite: String,
    @ColumnInfo(name="front_sprite") val frontSprite: String
    )

// Check to see if the pokemon is in the db if not insert
suspend fun loadPokemonByName(pokemonName: String, context: Context){
    val db = getDb(context)
    val pokemon = db.PokemonDao().getPokemonByName(pokemonName)
    if (pokemon == null){
        println("Getting $pokemonName from API")
        var data = getPokemonJSON(pokemonName)
        while (data == null){
            data = getPokemonJSON(pokemonName)
        }
        db.PokemonDao().insertPokemon(PokemonEntity(data.getInt("id"), pokemonName, data.getString("types"),
            data.getInt("base_maxHp"), data.getInt("base_attack"), data.getInt("base_defense"),
            data.getInt("base_special-attack"), data.getInt("base_special-defense"),
            data.getInt("base_speed"), data.getInt("base_exp_reward"), data.getString("back_sprite"),
            data.getString("front_sprite")))

        println("$pokemonName added to the database.")
    }
}

// Check to see if the pokemon is in the db if not insert
suspend fun loadPokemonById(pokemonId: Int, context: Context){
    withContext(Dispatchers.IO){
        val db = getDb(context)
        val pokemon = db.PokemonDao().getPokemonByName(pokemonId.toString())
        if (pokemon == null){
            var data = getPokemonJSON(pokemonId.toString())
            while (data == null){
                data = getPokemonJSON(pokemonId.toString())
            }
            db.PokemonDao().insertPokemon(PokemonEntity(pokemonId, data.getString("name"), data.getString("types"),
                data.getInt("base_maxHp"), data.getInt("base_attack"), data.getInt("base_defense"),
                data.getInt("base_special-attack"), data.getInt("base_special-defense"),
                data.getInt("base_speed"), data.getInt("base_exp_reward"), data.getString("back_sprite"),
                data.getString("front_sprite")))

            println("$pokemonId added to the database.")
        }
    }
}