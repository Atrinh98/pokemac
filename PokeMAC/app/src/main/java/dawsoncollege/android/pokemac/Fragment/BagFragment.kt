package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import dawsoncollege.android.pokemac.databinding.FragmentBagBinding
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class BagFragment : Fragment(R.layout.fragment_bag) {

    // Fields
    private lateinit var binding: FragmentBagBinding
    private lateinit var battleType: String
    private lateinit var enemy: CollectionEntity
    private lateinit var enemyPokemon: PokemonEntity
    private lateinit var db: PokemonDatabase

    /**
     * Creates the view and return the root of the binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBagBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Do binding in this class and the function to run code (default function).
     * Let's the user decide to either use a Potion or Pokeball.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.captureLayout.visibility = View.GONE

        battleType = arguments?.getString("battleType").toString()
        bindingVisible()
        bindingEvent()
    }

    //---------------Load/Fetch Enemy Info---------------------//

    /**
     * Load the enemy into the enemyPokemon variable.
     */
    private suspend fun loadEnemy(){
        db = getDb(requireActivity().applicationContext)
        enemy = db.CollectionDao().getPokemon(-1)!!
        loadPokemonByName(enemy.species, requireActivity().applicationContext)
        enemyPokemon = db.PokemonDao().getPokemonByName(enemy.species)!!
    }

    /**
     * Function to see if the enemy Pokemon gets captured or not.
     * @return Boolean to check if the capture was successful or not.
     */
    private fun capturePokemon(): Boolean{
        val randomRate = Random().nextInt(100)
        val captureRate = (1 - ( enemy.current_hp.toDouble() / getMaxHp(enemyPokemon.maxHp,
            getLevel(enemy.current_Exp)).toDouble()) ) * 100
        return randomRate <= captureRate
    }

    //---------------------------Binding---------------------------//

    /**
     * If the battletype is trainer then we hide the Pokeballs.
     */
    private fun bindingVisible(){
        if (battleType == "trainer") {
            binding.pokeballBtn.visibility = View.GONE
            binding.pokeballText.visibility = View.GONE
        }
    }

    /**
     * Set the binding for the buttons.
     */
    private fun bindingEvent(){
        // Event to capture the Pokemon.
        binding.pokeballBtn.setOnClickListener{
            lifecycleScope.launch(Dispatchers.IO){
                loadEnemy()
                withContext(Dispatchers.Main){
                    // If the capture is successful then we make the capture view visible.
                    if (capturePokemon()){
                        binding.pokemonNameInput.setText(enemy.species)
                        binding.captureLayout.visibility = View.VISIBLE
                    }
                    // Return to the battle.
                    else{
                        Toast.makeText(activity,"Pokemon escaped", Toast.LENGTH_SHORT).show()
                        val view = it
                        returnBattle(view)
                    }
                }
            }
        }
        // Event for when the Pokemon has been capture.
        binding.confirmButton.setOnClickListener{
            lifecycleScope.launch(Dispatchers.IO){
                // Save the wild Pokemon to the database.
                db = getDb(requireActivity().applicationContext)
                val pokemonName = binding.pokemonNameInput.text.toString()
                db.CollectionDao().insertCollection(CollectionEntity(pokemonName, enemy.species,
                    enemy.current_Exp, enemy.current_hp, enemy.level, enemy.move_1, enemy.move_2, enemy.move_3, enemy.move_4,
                    enemy.pp_1, enemy.pp_2, enemy.pp_3, enemy.pp_4,0))
                println("Pokemon added to the collection")

                // Delete the enemy Pokemom from the database.
                val delete = db.CollectionDao().getPokemon(-1)!!
                db.CollectionDao().deletePokemon(delete.collectionId)
                println("pokemon deleted")

                // Pokemon at position 1 gains the experience from the battle.
                gainXp("pokemon", db.CollectionDao().getPokemon(1)!!,
                    getPokemonCollection(enemy, requireContext().applicationContext),
                    requireContext().applicationContext)

                // Return to the main menu after everything.
                withContext(Dispatchers.Main){
                    it.findNavController().navigate(R.id.action_bagFragment_to_menuFragment)
                }
            }
        }
        // Event for when we use a Pokeball (heal by 20).
        binding.potionBtn.setOnClickListener{
            lifecycleScope.launch(Dispatchers.IO){
                db = getDb(requireActivity().applicationContext)
                val pokemon = db.CollectionDao().getPokemon(1)!!
                pokemon.current_hp += 20
                db.CollectionDao().updateCollection(pokemon)
                withContext(Dispatchers.Main){
                    Toast.makeText(activity,"Potion (+20)", Toast.LENGTH_SHORT).show()
                    val view = it
                    returnBattle(view)
                }
            }
        }
    }

    /**
     * Return to the battle after the event is done.
     * @param it the view associated to this fragment.
     */
    private fun returnBattle(it: View){
        if (battleType == "pokemon"){
            val bundle = Bundle()
            bundle.putBoolean("move", true)
            it.findNavController().navigate(R.id.action_bagFragment_to_pokemonBattleFragment, bundle)
        }
        else{
            val bundle = Bundle()
            bundle.putBoolean("move", true)
            bundle.putInt("trainer", arguments?.getInt("trainer")!!)
            it.findNavController().navigate(R.id.action_bagFragment_to_trainerBattleFragment, bundle)
        }
    }
}