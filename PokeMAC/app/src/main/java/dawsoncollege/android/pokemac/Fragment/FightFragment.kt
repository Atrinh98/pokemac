package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentFightBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.properties.Delegates
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class FightFragment: Fragment() {

    // Fields
    private lateinit var binding: FragmentFightBinding
    private lateinit var battleType: String
    private lateinit var ally: Pokemon
    private lateinit var enemy: Pokemon
    private lateinit var allyCollection: CollectionEntity
    private lateinit var enemyCollection: CollectionEntity
    private lateinit var enemyMove: MoveEntity
    private var moveNumber by Delegates.notNull<Int>()
    private lateinit var db: PokemonDatabase
    private var done: Boolean = false

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFightBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Load all the necessary information from ally and enemy Pokemons.
     * User decides which move he will attack with.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        battleType = arguments?.getString("battleType").toString()
        lifecycleScope.launch(Dispatchers.IO) {
            // Loading functions.
            db = getDb(requireActivity().applicationContext)
            allyCollection = db.CollectionDao().getPokemon(1)!!
            enemyCollection = db.CollectionDao().getPokemon(-1)!!
            loadAlly()
            loadEnemy()
            loadEnemyMove()
            // Binding Functions.
            withContext(Dispatchers.Main) {
                bindingAlly()
                bindingEnemy()
                bindingPP()
                bindingMoves()
                bindingEvent()
            }
            // Load the images at the end to not block the user from playing (if device is slow).
            val back = getBitmap(ally.back)
            val front = getBitmap(enemy.front)
            withContext(Dispatchers.Main){
                binding.pokemonImage.setImageBitmap(back)
                binding.enemyImage.setImageBitmap(front)
            }
        }
    }

    /**
     * Toast to show what the damage did to opposing Pokemon.
     */
    private suspend fun showToast(message: String) {
        withContext(Dispatchers.Main){
            Toast.makeText(requireActivity().applicationContext, message, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Save the information of the battle to the database.
     * Minus pp for the move use.
     * Hp for the Pokemons.
     * @param moveNumber the move number of the attacking Pokemon.
     * @param dmg the damage done to the enemy Pokemon or healed using Pokemon.
     * @param userInfo CollectionEntity of the attacking Pokemon.
     * @param enemyInfo Collection of the defending Pokemon.
     */
    private suspend fun savePokemon(moveNumber: Int, dmg: Int, userInfo: CollectionEntity, enemyInfo: CollectionEntity){
        when (moveNumber) {
            1 -> {
                userInfo.pp_1 -= 1
            }
            2 -> {
                userInfo.pp_2 -= 1
            }
            3 -> {
                userInfo.pp_3 -= 1
            }
            else -> {
                userInfo.pp_4 -= 1
            }
        }
        // Pokemon healed
        if (dmg > 0){
            userInfo.current_hp = ally.hp
        }
        else if (dmg == -9999){
            enemyInfo.current_hp = 0
        }
        // Pokemon attack
        else if (dmg < 0){
            enemyInfo.current_hp = enemy.hp
        }
        db.CollectionDao().updateCollection(enemyInfo)
        db.CollectionDao().updateCollection(userInfo)
    }

    //----------------------- Set Moves--------------------------------//

    /**
     * Sets the action to be executed.
     * The Pokemon with the higher speed attacks first.
     * Action: Attack the enemy with move #1
     */
    private suspend fun setMove1(){
        loadMove(allyCollection.move_1, requireActivity().applicationContext)
        val move = db.MoveDao().getMove(allyCollection.move_1)!!
        moveNumber = 1
        if (enemy.speed > ally.speed) {
            if (attackAlly()){
                attackEnemy(move, 1)
            }
        }
        else {
            if (attackEnemy(move, 1)) {
                attackAlly()
            }
        }
        done = true
    }

    /**
     * Sets the action to be executed.
     * The Pokemon with the higher speed attacks first.
     * Action: Attack the enemy with move #2
     */
    private suspend fun setMove2(){
        loadMove(allyCollection.move_2, requireActivity().applicationContext)
        val move = db.MoveDao().getMove(allyCollection.move_2)!!
        moveNumber = 2
        if (enemy.speed > ally.speed) {
            if (attackAlly()){
                attackEnemy(move, 2)

            }
        }
        else {
            if (attackEnemy(move, 2)) {
                attackAlly()

            }
        }
        done = true
    }

    /**
     * Sets the action to be executed.
     * The Pokemon with the higher speed attacks first.
     * Action: Attack the enemy with move #3
     */
    private suspend fun setMove3(){
        loadMove(allyCollection.move_3, requireActivity().applicationContext)
        val move = db.MoveDao().getMove(allyCollection.move_3)!!
        moveNumber = 3
        if (enemy.speed > ally.speed) {
            if (attackAlly()){
                attackEnemy(move, 3)
            }
        }
        else {
            if (attackEnemy(move, 3)) {
                attackAlly()
            }
        }
        done = true
    }

    /**
     * Sets the action to be executed.
     * The Pokemon with the higher speed attacks first.
     * Action: Attack the enemy with move #4
     */
    private suspend fun setMove4(){
        loadMove(allyCollection.move_4, requireActivity().applicationContext)
        val move = db.MoveDao().getMove(allyCollection.move_4)!!
        moveNumber = 4
        if (enemy.speed > ally.speed) {
            if (attackAlly()){
                attackEnemy(move, 4)
            }
        }
        else {
            if (attackEnemy(move, 4)) {
                attackAlly()
            }
        }
        done = true
    }

    //-------------------Attack (Ally)----------------------------//

    /**
     * Attack the enemy Pokemon with the selected move.
     * @return Boolean to check if the enemy Pokemon is alive or not.
     */
    private suspend fun attackEnemy(move: MoveEntity, moveNumber: Int): Boolean{
        val dmg = move.attack(enemy, ally, requireContext().applicationContext)
        savePokemon(moveNumber, dmg, allyCollection, enemyCollection)
        return showAllyDamageMessage(dmg)
    }

    /**
     * Show what the selected attack did to the enemy Pokemon.
     * @param dmg the damage dealt to the enemy Pokemon.
     * @return Boolean to check if enemy Pokemon is alive or not.
     */
    private suspend fun showAllyDamageMessage(dmg: Int): Boolean{
        // Pokemon healed
        if (dmg > 0){
            val message = resources.getString(R.string.ally_attack_heal, dmg)
            showToast(message)
            return true
        }
        // Pokemon miss attack
        else if (dmg == 0){
            val message = resources.getString(R.string.ally_attack_miss)
            showToast(resources.getString(R.string.ally_attack_miss))
            return true
        }
        // Pokemon died
        else if (dmg == -9999){
            val message = resources.getString(R.string.ally_attack_faint)
            showToast(message)
            // Load other pokemons + gain xp
            if (battleType == "trainer"){
                gainXp("trainer", allyCollection, enemy, requireContext().applicationContext)
                getNextEnemy(requireActivity().applicationContext)
                return false
            }
            // Pokemon battle so return to the menu
            else{
                gainXp("pokemon", allyCollection, enemy, requireContext().applicationContext)
                return false
            }
        }
        // Pokemon attack
        else{
            val message = resources.getString(R.string.ally_attack_damage, dmg)
            showToast(message)
            return true
        }
    }


    //--------------------Attack (Enemy)----------------------------//

    /**
     * Attack the ally Pokemon with the enemy move loaded.
     * @return Boolean to check if the ally Pokemon is alive or not.
     */
    private suspend fun attackAlly(): Boolean{
        val dmg = enemyMove.attack(ally, enemy, requireContext().applicationContext)
        savePokemon(moveNumber, dmg, enemyCollection, allyCollection)
        return showEnemyDamageMessage(dmg)
    }

    /**
     * Show what the enemy attack did to the ally Pokemon.
     * @param dmg the damage dealt to the ally Pokemon.
     * @return Boolean to check if ally Pokemon is alive or not.
     */
    private suspend fun showEnemyDamageMessage(dmg: Int): Boolean{
        // Pokemon healed
        if (dmg > 0){
            showToast(resources.getString(R.string.enemy_attack_heal, dmg))
            return true
        }
        // Pokemon miss attack
        else if (dmg == 0){
            showToast(resources.getString(R.string.enemy_attack_miss))
            return true
        }
        // Pokemon died
        else if (dmg == -9999){
            showToast(resources.getString(R.string.enemy_attack_faint))
            getNextAlly(requireContext().applicationContext)
            return false
        }
        // Pokemon attack
        else{
            showToast(resources.getString(R.string.enemy_attack_damage, dmg))
            return true
        }
    }

    //----------------------- Load Enemy Move----------------------------//

    /**
     * Load the enemy move that will be use.
     */
    private suspend fun loadEnemyMove(){
        var attack = false
        var moveName = ""
        while(!attack){
            moveNumber = Random.nextInt(4)
            when (moveNumber) {
                1 -> {
                    if (enemyCollection.pp_1 > 0){
                        moveName = enemy.move1
                        attack = true
                    }
                }
                2 -> {
                    if (enemyCollection.pp_2 > 0){
                        moveName = enemy.move2
                        attack = true
                    }
                }
                3 -> {
                    if (enemyCollection.pp_3 > 0){
                        moveName = enemy.move3
                        attack = true
                    }
                }
                else -> {
                    if (enemyCollection.pp_4 > 0){
                        moveName = enemy.move4
                        attack = true
                    }
                }
            }
        }
        loadMove(moveName, requireActivity().applicationContext)
        enemyMove = db.MoveDao().getMove(moveName)!!
    }

    //-------------------------- Load Pokemons----------------------------------//

    /**
     * Load the enemy Pokemon.
     */
    private suspend fun loadEnemy() {
        val enemyDb = db.CollectionDao().getPokemon(-1)!!
        enemy = getPokemonCollection(enemyDb, requireContext().applicationContext)
        enemy.setStatValues()
        enemy.hp = enemyDb.current_hp
    }

    /**
     * Load the ally Pokemon.
     */
    private suspend fun loadAlly(){
        val allyDb = db.CollectionDao().getPokemon(1)!!
        ally = getPokemonCollection(allyDb, requireContext().applicationContext)
        ally.setStatValues()
        ally.hp = allyDb.current_hp
    }

    //--------------------Binding------------------------//

    /**
     * Set the event binding for each button.
     * Event (Attack the enemy Pokemon with the move selected).
     * The while is waiting for the attack to be done
     * since the onSetListener cannot be part of the same lifecycleScope.
     */
    private fun bindingEvent(){
        binding.move1.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                setMove1()
                lifecycleScope.launch(Dispatchers.Main){
                    while (!done){

                    }
                    val view = it
                    returnBattle(view)
                }
            }
        }
        binding.move2.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                setMove2()
                lifecycleScope.launch(Dispatchers.Main){
                    while (!done){

                    }
                    val view = it
                    returnBattle(view)
                }
            }
        }
        binding.move3.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                setMove3()
                lifecycleScope.launch(Dispatchers.Main){
                    while (!done){

                    }
                    val view = it
                    returnBattle(view)
                }
            }
        }
        binding.move4.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                setMove4()
                lifecycleScope.launch(Dispatchers.Main){
                    while (!done){

                    }
                    val view = it
                    returnBattle(view)
                }
            }
        }
    }

    /**
     * Set the binding for the ally Pokemon.
     */
    private fun bindingAlly(){
        binding.pokemonName.text = ally.name
        binding.pokemonLvl.text = ally.level.toString()
        binding.pokemonHp.max = ally.maxHp
        binding.pokemonHp.progress = ally.hp
        binding.pokemonHpText.text = resources.getString(
            R.string.hp_text, ally.hp,
            ally.maxHp)
    }

    /**
     * Set the binding for the enemy Pokemon.
     */
    private fun bindingEnemy(){
        binding.enemyName.text = enemy.name
        binding.enemyLvl.text = enemy.level.toString()
        binding.enemyHp.max = enemy.maxHp
        binding.enemyHp.progress = enemy.hp
        binding.enemyHpText.text = resources.getString(
            R.string.hp_text, enemy.hp,
            enemy.maxHp)
    }

    /**
     * Set the binding to see how many pp each moves have.
     */
    private suspend fun bindingPP(){
        var counter = 0
        binding.pp1.text   = resources.getString(
            R.string.pp_text,
            allyCollection.pp_1)
        if (allyCollection.pp_1 <= 0){
            binding.pp1.visibility = View.GONE
            binding.move1.visibility = View.GONE
            counter++
        }
        binding.pp2.text   = resources.getString(
            R.string.pp_text,
            allyCollection.pp_2)
        if (allyCollection.pp_2 <= 0){
            binding.pp2.visibility = View.GONE
            binding.move2.visibility = View.GONE
            counter++
        }
        binding.pp3.text   = resources.getString(
            R.string.pp_text,
            allyCollection.pp_3)
        if (allyCollection.pp_3 <= 0){
            binding.pp3.visibility = View.GONE
            binding.move3.visibility = View.GONE
            counter++
        }
        binding.pp4.text   = resources.getString(
            R.string.pp_text,
            allyCollection.pp_4)
        if (allyCollection.pp_4 <= 0){
            binding.pp4.visibility = View.GONE
            binding.move4.visibility = View.GONE
            counter++
        }
        // When no more moves are available return back to battle.
        if (counter == 4){
            Toast.makeText(activity,"No more moves for this pokemon",Toast.LENGTH_SHORT).show()
            binding.backButton.visibility = View.VISIBLE
            binding.backButton.setOnClickListener {
                val view = it
                returnBattle(view)
            }
        }
    }

    /**
     * Set the bindings for move names.
     */
    private fun bindingMoves(){
        binding.move1.text = allyCollection.move_1
        binding.move2.text = allyCollection.move_2
        binding.move3.text = allyCollection.move_3
        binding.move4.text = allyCollection.move_4
    }

    /**
     * Return to battle trainer or Pokemon battle.
     * @param view the view associated to this fragment.
     */
    private fun returnBattle(view: View){
        if (battleType == "pokemon"){
            view.findNavController().navigate(R.id.action_fightFragment_to_pokemonBattleFragment)
        }
        else{
            val bundle = Bundle()
            bundle.putInt("trainer", arguments?.getInt("trainer")!!)
            view.findNavController().navigate(R.id.action_fightFragment_to_trainerBattleFragment, bundle)
        }
    }
}