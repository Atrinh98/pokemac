package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.Database.getDb
import dawsoncollege.android.pokemac.R
import dawsoncollege.android.pokemac.databinding.FragmentRunBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class RunFragment : Fragment(R.layout.fragment_run){

    // Fields
    private lateinit var binding: FragmentRunBinding


    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRunBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Set the action event to the button bindings.
     * Lets the user choose between Running or Going back to battle.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                val db = getDb(requireActivity().applicationContext)
                val enemy = db.CollectionDao().getPokemon(-1)!!
                db.CollectionDao().deletePokemon(enemy.collectionId)
                withContext(Dispatchers.Main){
                    Toast.makeText(activity, "You ran away!!", Toast.LENGTH_SHORT).show()
                    it.findNavController().navigate(R.id.action_runFragment_to_menuFragment)
                }
            }
        }
        binding.backButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("move", false)
            it.findNavController().navigate(R.id.action_runFragment_to_pokemonBattleFragment, bundle)
        }
    }
}