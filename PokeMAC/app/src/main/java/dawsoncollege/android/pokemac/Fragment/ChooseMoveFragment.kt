package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.Database.CollectionEntity
import dawsoncollege.android.pokemac.Database.PokemonDatabase
import dawsoncollege.android.pokemac.Database.getDb
import dawsoncollege.android.pokemac.Database.getMovePP
import dawsoncollege.android.pokemac.R
import dawsoncollege.android.pokemac.databinding.FragmentChooseMoveBinding
import dawsoncollege.android.pokemac.getLevel
import dawsoncollege.android.pokemac.getPokemonNewMove
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class ChooseMoveFragment : Fragment(R.layout.fragment_choose_move) {

    // Fields
    private lateinit var binding: FragmentChooseMoveBinding
    private lateinit var battleType: String
    private lateinit var db: PokemonDatabase
    private lateinit var pokemon: CollectionEntity
    private lateinit var newMove: String

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChooseMoveBinding.inflate(layoutInflater)
        return binding.root
    }


    /**
     * Bindings are done here and the function runs from here (default function).
     * User decides if he wants to keep all the moves or override one when a Pokemon levels up.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        battleType = arguments?.getString("battleType").toString()
        println(battleType)
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireContext().applicationContext)

            pokemon = db.CollectionDao().getPokemon(1)!!
            var moveName = getPokemonNewMove(pokemon.species, getLevel(pokemon.current_Exp))
            while (moveName == null){
                moveName = getPokemonNewMove(pokemon.species, getLevel(pokemon.current_Exp))
            }

            newMove = moveName

            withContext(Dispatchers.Main){
                bindingName()
                bindingEvent()
            }
        }
    }

    /**
     * Set the bindings for the moves' name.
     */
    private fun bindingName(){
        binding.move1.text = pokemon.move_1
        binding.move2.text = pokemon.move_2
        binding.move3.text = pokemon.move_3
        binding.move4.text = pokemon.move_4
        binding.newMove.text = resources.getString(R.string.new_move, newMove)
        binding.levelUp.text = resources.getString(R.string.level_up, pokemon.name)
    }

    /**
     * Set the event for the buttons.
     * When the user clicks on a move, the Pokemon will override it with the new move (learns a new move).
     */
    private suspend fun bindingEvent(){
        binding.move1.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                pokemon.move_1 = newMove
                pokemon.pp_1 = getMovePP(newMove, requireContext().applicationContext)
                db.CollectionDao().updateCollection(pokemon)
                setLevelCorrectly()
                withContext(Dispatchers.Main){
                    returnBattle(it)
                }
            }
        }
        binding.move2.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                pokemon.move_2 = newMove
                pokemon.pp_2 = getMovePP(newMove, requireContext().applicationContext)
                db.CollectionDao().updateCollection(pokemon)
                setLevelCorrectly()
                withContext(Dispatchers.Main){
                    returnBattle(it)
                }
            }
        }
        binding.move3.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                pokemon.move_3 = newMove
                pokemon.pp_3 = getMovePP(newMove, requireContext().applicationContext)
                db.CollectionDao().updateCollection(pokemon)
                setLevelCorrectly()
                withContext(Dispatchers.Main){
                    returnBattle(it)
                }
            }
        }
        binding.move4.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                pokemon.move_4 = newMove
                pokemon.pp_4 = getMovePP(newMove, requireContext().applicationContext)
                db.CollectionDao().updateCollection(pokemon)
                setLevelCorrectly()
                withContext(Dispatchers.Main){
                    returnBattle(it)
                }
            }
        }
        // No override to any moves.
        binding.confirm.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                setLevelCorrectly()
                withContext(Dispatchers.Main){
                    returnBattle(it)
                }
            }

        }
    }

    /**
     * Puts the corrects level to the evolving Pokemon
     */
    private suspend fun setLevelCorrectly(){
        val first = db.CollectionDao().getPokemon(1)!!
        first.level = getLevel(first.current_Exp)
        db.CollectionDao().updateCollection(first)
    }

    /**
     * Return to the battle after action has been selected.
     * @param view the view associated with the fragment.
     */
    private fun returnBattle(view: View){
        if (battleType == "pokemon"){
            view.findNavController().navigate(R.id.action_chooseMoveFragment_to_pokemonBattleFragment)
        }
        else{
            val bundle = Bundle()
            bundle.putInt("trainer", arguments?.getInt("trainer")!!)
            view.findNavController().navigate(R.id.action_chooseMoveFragment_to_trainerBattleFragment, bundle)
        }
    }
}