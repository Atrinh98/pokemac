package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentSwapBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class SwapFragment : Fragment(R.layout.fragment_swap){

    // Fields
    private lateinit var binding: FragmentSwapBinding
    private var click: CLICK = CLICK.NONE
    private lateinit var battleType: String
    private lateinit var db: PokemonDatabase
    private var col1: CollectionEntity? = null
    private var col2: CollectionEntity? = null
    private var col3: CollectionEntity? = null
    private var col4: CollectionEntity? = null
    private var col5: CollectionEntity? = null
    private var col6: CollectionEntity? = null
    private var pokemon1: PokemonEntity? = null
    private var pokemon2: PokemonEntity? = null
    private var pokemon3: PokemonEntity? = null
    private var pokemon4: PokemonEntity? = null
    private var pokemon5: PokemonEntity? = null
    private var pokemon6: PokemonEntity? = null

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSwapBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Set the binding for the Pokemon container to swap with Pokemon at position 1.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        battleType = arguments?.getString("battleType").toString()
        setContainerGone()
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireActivity().applicationContext)
            getPokemon()
            withContext(Dispatchers.Main){
                bindingPokemon()
                bindingEvent()
            }
            // Load the images at the end for slow devices.
            loadBitmap()
        }
    }

    /**
     * When a container has been click, the number represent each container aside from one.
     */
    private enum class CLICK {TWO, THREE, FOUR, FIVE, SIX, NONE}

    /**
     * Swap the selected resting Pokemon with the battling one.
     */
    private suspend fun swapPokemon(){
        when (click) {
            CLICK.TWO -> {
                val rest = db.CollectionDao().getPokemon(2)!!
                rest.position = 1
                val battle = db.CollectionDao().getPokemon(1)!!
                battle.position = 2
                db.CollectionDao().updateCollection(rest)
                db.CollectionDao().updateCollection(battle)
            }
            CLICK.THREE -> {
                val rest = db.CollectionDao().getPokemon(3)!!
                rest.position = 1
                val battle = db.CollectionDao().getPokemon(1)!!
                battle.position = 3
                db.CollectionDao().updateCollection(rest)
                db.CollectionDao().updateCollection(battle)
            }
            CLICK.FOUR -> {
                val rest = db.CollectionDao().getPokemon(4)!!
                rest.position = 1
                val battle = db.CollectionDao().getPokemon(1)!!
                battle.position = 4
                db.CollectionDao().updateCollection(rest)
                db.CollectionDao().updateCollection(battle)
            }
            CLICK.FIVE -> {
                val rest = db.CollectionDao().getPokemon(5)!!
                rest.position = 1
                val battle = db.CollectionDao().getPokemon(1)!!
                battle.position = 5
                db.CollectionDao().updateCollection(rest)
                db.CollectionDao().updateCollection(battle)
            }
            CLICK.SIX -> {
                val rest = db.CollectionDao().getPokemon(6)!!
                rest.position = 1
                val battle = db.CollectionDao().getPokemon(1)!!
                battle.position = 6
                db.CollectionDao().updateCollection(rest)
                db.CollectionDao().updateCollection(battle)
            }
            else -> {}
        }
    }

    //--------------Load Pokemon Info---------------------------//

    /**
     * Load the Pokemon team and assigns them to the global variable colx and pokemonx.
     */
    private suspend fun getPokemon(){
        col1 = db.CollectionDao().getPokemon(1)
        if (col1 != null){
            loadPokemonByName(col1!!.species, requireContext().applicationContext)
            pokemon1 = db.PokemonDao().getPokemonByName(col1!!.species)!!
        }
        col2 = db.CollectionDao().getPokemon(2)
        if (col2 != null){
            loadPokemonByName(col2!!.species, requireContext().applicationContext)
            pokemon2 = db.PokemonDao().getPokemonByName(col2!!.species)!!
        }
        col3 = db.CollectionDao().getPokemon(3)
        if (col3 != null){
            loadPokemonByName(col3!!.species, requireContext().applicationContext)
            pokemon3 = db.PokemonDao().getPokemonByName(col3!!.species)!!
        }
        col4 = db.CollectionDao().getPokemon(4)
        if (col4 != null){
            loadPokemonByName(col4!!.species, requireContext().applicationContext)
            pokemon4 = db.PokemonDao().getPokemonByName(col4!!.species)!!
        }
        col5 = db.CollectionDao().getPokemon(5)
        if (col5 != null){
            loadPokemonByName(col5!!.species, requireContext().applicationContext)
            pokemon5 = db.PokemonDao().getPokemonByName(col5!!.species)!!
        }
        col6 = db.CollectionDao().getPokemon(6)
        if (col6 != null){
            loadPokemonByName(col6!!.species, requireContext().applicationContext)
            pokemon6 = db.PokemonDao().getPokemonByName(col6!!.species)!!
        }
    }


    /**
     * Load the image for every Pokemon in the team.
     */
    private suspend fun loadBitmap(){
        if (col1 != null){
            val front = getBitmap(pokemon1!!.frontSprite)
            withContext(Dispatchers.Main){
                binding.image1.setImageBitmap(front)
            }
        }
        if (col2 != null){
            val front = getBitmap(pokemon2!!.frontSprite)
            withContext(Dispatchers.Main){
                binding.image2.setImageBitmap(front)
            }
        }
        if (col3 != null){
            val front = getBitmap(pokemon3!!.frontSprite)
            withContext(Dispatchers.Main){
                binding.image3.setImageBitmap(front)
            }
        }
        if (col4 != null){
            val front = getBitmap(pokemon4!!.frontSprite)
            withContext(Dispatchers.Main){
                binding.image4.setImageBitmap(front)
            }
        }
        if (col5 != null){
            val front = getBitmap(pokemon5!!.frontSprite)
            withContext(Dispatchers.Main){
                binding.image5.setImageBitmap(front)
            }
        }
        if (col6 != null){
            val front = getBitmap(pokemon6!!.frontSprite)
            withContext(Dispatchers.Main){
                binding.image6.setImageBitmap(front)
            }
        }
    }

    //------------------Binding-------------------------//

    /**
     * Make all the containers gone.
     */
    private fun setContainerGone(){
        binding.container1.visibility = View.GONE
        binding.container2.visibility = View.GONE
        binding.container3.visibility = View.GONE
        binding.container4.visibility = View.GONE
        binding.container5.visibility = View.GONE
        binding.container6.visibility = View.GONE
    }

    /**
     * Set the binding for the Pokemons are in the team.
     * Set the hp bar, name and level.
     * Set the container visible for the Pokemon that are in the team.
     */
    private fun bindingPokemon(){
        if (col1 != null){
            binding.container1.visibility = View.VISIBLE
            binding.name1.text = col1!!.name
            binding.hp1.max = getMaxHp(pokemon1!!.maxHp, getLevel(col1!!.current_Exp))
            binding.hp1.progress = col1!!.current_hp
            binding.lvl1.text = getLevel(col1!!.current_Exp).toString()
            if (col1!!.current_hp == 0){
                binding.state1.text = resources.getString(R.string.swap_dead)
            }
        }
        if (col2 != null){
            binding.container2.visibility = View.VISIBLE
            binding.name2.text = col2!!.name
            binding.hp2.max = getMaxHp(pokemon2!!.maxHp, getLevel(col2!!.current_Exp))
            binding.hp2.progress = col2!!.current_hp
            binding.lvl2.text = getLevel(col2!!.current_Exp).toString()
            if (col2!!.current_hp == 0){
                binding.state2.text = resources.getString(R.string.swap_dead)
            }
        }
        if (col3 != null){
            binding.container3.visibility = View.VISIBLE
            binding.name3.text = col3!!.name
            binding.hp3.max = getMaxHp(pokemon3!!.maxHp, getLevel(col3!!.current_Exp))
            binding.hp3.progress = col3!!.current_hp
            binding.lvl3.text = getLevel(col3!!.current_Exp).toString()
            if (col3!!.current_hp == 0){
                binding.state3.text = resources.getString(R.string.swap_dead)
            }
        }
        if (col4 != null){
            binding.container4.visibility = View.VISIBLE
            binding.name4.text = col4!!.name
            binding.hp4.max = getMaxHp(pokemon4!!.maxHp, getLevel(col4!!.current_Exp))
            binding.hp4.progress = col4!!.current_hp
            binding.lvl4.text = getLevel(col4!!.current_Exp).toString()
            if (col4!!.current_hp == 0){
                binding.state4.text = resources.getString(R.string.swap_dead)
            }
        }
        if (col5 != null){
            binding.container5.visibility = View.VISIBLE
            binding.name5.text = col5!!.name
            binding.hp5.max = getMaxHp(pokemon5!!.maxHp, getLevel(col5!!.current_Exp))
            binding.hp5.progress = col5!!.current_hp
            binding.lvl5.text = getLevel(col5!!.current_Exp).toString()
            if (col5!!.current_hp == 0){
                binding.state5.text = resources.getString(R.string.swap_dead)
            }
        }
        if (col6 != null){
            binding.container6.visibility = View.VISIBLE
            binding.name6.text = col6!!.name
            binding.hp6.max = getMaxHp(pokemon6!!.maxHp, getLevel(col6!!.current_Exp))
            binding.hp6.progress = col6!!.current_hp
            binding.lvl6.text = getLevel(col6!!.current_Exp).toString()
            if (col6!!.current_hp == 0){
                binding.state6.text = resources.getString(R.string.swap_dead)
            }
        }
    }


    /**
     * Set the action event for each container.
     * Action: Swap the resting Pokemon with the battling one.
     */
    private fun bindingEvent(){
        binding.container1.setOnClickListener{
            val view : View = it
            if (click != CLICK.NONE){
                lifecycleScope.launch(Dispatchers.IO){
                    swapPokemon()
                    withContext(Dispatchers.Main){
                        returnBattle(view)
                    }
                }
            }
        }
        binding.container2.setOnClickListener {
            if (click != CLICK.TWO){
                cancelSelected()
                click = CLICK.TWO
                binding.select2.visibility = View.VISIBLE
                binding.container2.
                setBackgroundColor(resources.getColor(R.color.select_swap))
            }
            else if (click == CLICK.TWO){
                click = CLICK.NONE
                binding.select2.visibility = View.INVISIBLE
                binding.container2.
                setBackgroundColor(resources.getColor(R.color.white))
            }
        }
        binding.container3.setOnClickListener {
            if (click != CLICK.THREE){
                cancelSelected()
                click = CLICK.THREE
                binding.select3.visibility = View.VISIBLE
                binding.container3.
                setBackgroundColor(resources.getColor(R.color.select_swap))
            }
            else if (click == CLICK.THREE){
                click = CLICK.NONE
                binding.select3.visibility = View.INVISIBLE
                binding.container3.
                setBackgroundColor(resources.getColor(R.color.white))
            }
        }
        binding.container4.setOnClickListener {
            if (click != CLICK.FOUR){
                cancelSelected()
                click = CLICK.FOUR
                binding.select4.visibility = View.VISIBLE
                binding.container4.
                setBackgroundColor(resources.getColor(R.color.select_swap))
            }
            else if (click == CLICK.FOUR){
                click = CLICK.NONE
                binding.select4.visibility = View.INVISIBLE
                binding.container4.
                setBackgroundColor(resources.getColor(R.color.white))
            }
        }
        binding.container5.setOnClickListener {
            if (click != CLICK.FIVE){
                cancelSelected()
                click = CLICK.FIVE
                binding.select5.visibility = View.VISIBLE
                binding.container5.
                setBackgroundColor(resources.getColor(R.color.select_swap))
            }
            else if (click == CLICK.FIVE){
                click = CLICK.NONE
                binding.select5.visibility = View.INVISIBLE
                binding.container5.
                setBackgroundColor(resources.getColor(R.color.white))
            }
        }
        binding.container6.setOnClickListener {
            if (click != CLICK.SIX){
                cancelSelected()
                click = CLICK.SIX
                binding.select6.visibility = View.VISIBLE
                binding.container6.
                setBackgroundColor(resources.getColor(R.color.select_swap))
            }
            else if (click == CLICK.SIX){
                click = CLICK.NONE
                binding.select6.visibility = View.INVISIBLE
                binding.container6.
                setBackgroundColor(resources.getColor(R.color.white))
            }
        }
    }

    /**
     * Reset the view to the beginning, nothing is selected and no click.
     */
    private fun cancelSelected(){
        when (click) {
            CLICK.TWO -> {
                binding.select2.visibility = View.INVISIBLE
                binding.container2.
                setBackgroundColor(resources.getColor(R.color.swap02))
            }
            CLICK.THREE -> {
                binding.select3.visibility = View.INVISIBLE
                binding.container3.
                setBackgroundColor(resources.getColor(R.color.swap03))
            }
            CLICK.FOUR -> {
                binding.select4.visibility = View.INVISIBLE
                binding.container4.
                setBackgroundColor(resources.getColor(R.color.swap04))
            }
            CLICK.FIVE -> {
                binding.select5.visibility = View.INVISIBLE
                binding.container5.
                setBackgroundColor(resources.getColor(R.color.swap05))
            }
            CLICK.SIX -> {
                binding.select6.visibility = View.INVISIBLE
                binding.container6.
                setBackgroundColor(resources.getColor(R.color.swap06))
            }
            else -> {}
        }
    }

    /**
     * Return to battle when the action is done.
     * @param it the view that is associated to this fragment.
     */
    private fun returnBattle(it: View){
        if (battleType == "pokemon"){
            val bundle = Bundle()
            bundle.putBoolean("move", true)
            it.findNavController().navigate(R.id.action_swapFragment_to_pokemonBattleFragment, bundle)
        }
        else{
            val bundle = Bundle()
            bundle.putBoolean("move", true)
            bundle.putInt("trainer", arguments?.getInt("trainer")!!)
            it.findNavController().navigate(R.id.action_swapFragment_to_trainerBattleFragment, bundle)
        }
    }
}