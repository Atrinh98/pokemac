package dawsoncollege.android.pokemac.Database

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import dawsoncollege.android.pokemac.Pokemon
import dawsoncollege.android.pokemac.getMoveJSON
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Entity
data class MoveEntity (
    @PrimaryKey val name: String,
    @ColumnInfo(name="accuracy") val accuracy: Int,
    @ColumnInfo(name="PP") val max_PP: Int,
    @ColumnInfo(name="power") val power: Int,
    @ColumnInfo(name="heal") val heal: Int,
    @ColumnInfo(name="damage_class") val damage_class: String,
    @ColumnInfo(name="type") val type: String,
    @ColumnInfo(name="target") val target:String,
){

    /**
     * Attack the enemy Pokemon or heal the Pokemon using the move.
     * @param enemy Pokemon getting attacked.
     * @param user Pokemon attacking (using this move).
     * @param context to have access to our database.
     */
    suspend fun attack(enemy: Pokemon, user: Pokemon, context: Context): Int {
        // Healing attack
        if(this.power == 0){
            println("Healing Attack.")
            val maxHp = user.maxHp
            val heal = ((maxHp * this.heal).toDouble() / 100.0).toInt()
            user.hp = user.hp + heal
            if (user.hp > user.maxHp){
                user.hp = user.maxHp
            }
            return heal
        }
        // Attack (power more than 0)
        else{
            // Attack if the random is lower than the accuracy.
            if (Random.nextInt(100) <= this.accuracy){
                var dmg = getDamage(enemy, user)
                if (user.type1 == this.type || user.type2 == this.type){
                    dmg *= 1.5
                }
                 // Check the type of defending Pokemon to get type multiplier.
                dmg *= getTypeMultiplier(this.type, enemy.type1, context)
                if (enemy.type2 != null){
                    dmg *= getTypeMultiplier(this.type, enemy.type2!!, context)
                }
                enemy.hp -= dmg.toInt()

                // Pokemon fainted (-9999 use for if statements).
                if (enemy.hp < 0){
                    return -9999
                }
               return (dmg.toInt() * -1)
            }
            // Attack miss.
            else{
                return 0
            }
        }
    }

    /**
     * Get the damage depending on the type of attack (physical or special).
     * @param enemy Pokemon getting attack, take its defense stats.
     * @param user Pokemon attacking, take its attack stats.
     * @return Get the damage depending on the type of attack.
     */
    private fun getDamage(enemy: Pokemon, user: Pokemon): Double{
        // Physical.
        return if (this.damage_class == "PHYSICAL"){
            ((((2 * user.level).toDouble() / 5.0) + 2) /50.0) * this.power *
                    (user.atk.toDouble() / enemy.def.toDouble()) + 2
        }
        // Special.
        else{
            ((((2 * user.level).toDouble() / 5.0) + 2) /50.0) * this.power *
                    (user.specialAtk.toDouble() / enemy.specialDef.toDouble()) + 2
        }
    }
}


/**
 * Load the move if it not in the database.
 * @param moveName The name of the move to load.
 * @param context to have access to the database.
 */
suspend fun loadMove(moveName: String, context: Context) {
    // Get the database and checks if the move exists.
    val db = getDb(context)
    val move = db.MoveDao().getMove(moveName)

    // If it does not exist then we fetch from API.
    if (move == null) {
        println("Getting $moveName info from API.")
        var data = getMoveJSON(moveName)
        while (data == null) {
            data = getMoveJSON(moveName)
        }

        // Inserting move to the database.
        db.MoveDao().insertMove(
            MoveEntity(
                moveName, data.getInt("accuracy"), data.getInt("max_PP"),
                data.getInt("power"), data.getInt("heal"), data.getString("damage_class"),
                data.getString("type").lowercase(), data.getString("target"))
        )
        println("$moveName added to the database.")
    }
}

/**
 * Returns the max pp of a move.
 * @param moveName name of the move.
 * @param context to have access to our database.
 * @return Get the max pp of a move.
 */
suspend fun getMovePP(moveName: String, context: Context): Int{
    // Have this as validation in case a Pokemon has less than 4 moves (Weedle for example when he is too low level).
    if (moveName == "null"){
        return 0
    }
    else{
        println("$moveName getting max pp.")
        val db = getDb(context)
        loadMove(moveName, context)
        val move = db.MoveDao().getMove(moveName)!!
        return move.max_PP
    }
}