package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.Database.getDb
import dawsoncollege.android.pokemac.R
import dawsoncollege.android.pokemac.databinding.FragmentTitleBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class TitleFragment : Fragment(R.layout.fragment_title) {

    // Fields
    private lateinit var binding: FragmentTitleBinding

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = FragmentTitleBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Set the action event to continue through the game.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch(Dispatchers.IO){
            val db = getDb(requireContext().applicationContext)

            val team = db.CollectionDao().getTeam()
            val collection = db.CollectionDao().getCollection()
            withContext(Dispatchers.Main){
                // Start a new game if no Pokemon in team or collection.
                if (team.isEmpty() && collection.isEmpty() ){
                    binding.startButton.setOnClickListener {
                        it.findNavController().navigate(R.id.action_titleFragment_to_introFragment)
                    }
                }
                // Continue to main menu.
                else{
                    binding.startButton.setOnClickListener {
                        it.findNavController().navigate(R.id.action_titleFragment_to_menuFragment)
                    }
                }
            }
        }
    }
}