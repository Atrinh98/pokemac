package dawsoncollege.android.pokemac.Database

import androidx.room.*

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Dao
interface CollectionDao {

    /**
     * Insert query for CollectionEntity.
     * @param collection CollectionEntity object to insert in the database.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollection(vararg collection: CollectionEntity)

    /**
     * Update query for CollectionEntity.
     * @param collection CollectionEntity object to update the database.
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateCollection(vararg collection: CollectionEntity)

    /**
     * Query to get all the Pokemon collection (team not including).
     * @return MutableList of CollectionEntity which is the Pokemon Collection.
     */
    @Query("Select * from CollectionEntity where position = 0")
    suspend fun getCollection(): MutableList<CollectionEntity>

    /**
     * Query to get all the user's Pokemon team.
     * @return MutableList of CollectionEntity which is the user's Pokemon team.
     */
    @Query("Select * from CollectionEntity where position != 0 and position > 0")
    suspend fun getTeam(): MutableList<CollectionEntity>

    /**
     * Query to get all the enemy trainer's Pokemon team.
     * @return MutableList of CollectionEntity which is the enemy trainer's Pokemon team.
     */
    @Query("Select * from CollectionEntity where position != 0 and position < 0")
    suspend fun getEnemyTeam(): MutableList<CollectionEntity>

    /**
     * Query to get a Pokemon at a certain position (use for Pokemon not in the collection).
     * @param position The position to get our Pokemon.
     * @return CollectionEntity which is the Pokemon at that position or null if it does not exist.
     */
    @Query("Select * from CollectionEntity where position = :position")
    suspend fun getPokemon(position: Int): CollectionEntity?


    /**
     * Query to get the collection entry with the same id (primary key).
     * Mainly use with the delete query.
     * @param id collection id which is the primary key for the collection entry.
     * @return collection entry with the same id (primary key).
     */
    @Query("Select * from CollectionEntity where collectionId = :id")
    suspend fun getPokemonWithId(id: Int): CollectionEntity


    /**
     * Delete query to remove an entry from the database.
     * @param id collection id which is the primary key for the collection entry.
     */
    @Query("Delete from CollectionEntity where collectionId = :id")
    suspend fun deletePokemon(id: Int)
}