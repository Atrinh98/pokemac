package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentPokeCenterBinding
import kotlinx.coroutines.*

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class PokeCenterFragment: Fragment(R.layout.fragment_poke_center) {

    // Fields
    private lateinit var binding : FragmentPokeCenterBinding
    private lateinit var db: PokemonDatabase
    private var progressStatus = 0
    private val handler: Handler = Handler()

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokeCenterBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Show a progress bar to the user and restore all Pokemon in the team to max Hp and all moves to Max PP.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.backButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_pokeCenterFragment_to_menuFragment)
        }
        binding.progressBar.max = 100
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireActivity().applicationContext)
            restorePokemon()
            withContext(Dispatchers.Main){
                while (progressStatus < 100) {
                    progressStatus += 5
                    // Update the progress bar and display the
                    binding.progressBar.progress = progressStatus
                    binding.textProgress.text = (progressStatus.toString() + "/" + binding.progressBar.max)
                    if (progressStatus == 100){
                        binding.healText.visibility = View.VISIBLE
                        binding.backButton.visibility = View.VISIBLE
                    }
                    try {
                        // Sleep for 200 milliseconds.
                        delay(200)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    /**
     * Restores the Pokemon to max Hp and their moves to max PP.
     */
    private suspend fun restorePokemon(){
        val team = db.CollectionDao().getTeam()
        // Get the max hp for each pokemon and the max pp for each move
        for (pokemon in team){
            val hp = returnPokemonHp(pokemon.species)
            pokemon.current_hp = getMaxHp(hp, getLevel(pokemon.current_Exp))
            pokemon.pp_1 = getMovePP(pokemon.move_1, requireActivity().applicationContext)
            pokemon.pp_2 = getMovePP(pokemon.move_2, requireActivity().applicationContext)
            pokemon.pp_3 = getMovePP(pokemon.move_3, requireActivity().applicationContext)
            pokemon.pp_4 = getMovePP(pokemon.move_4, requireActivity().applicationContext)
            db.CollectionDao().updateCollection(pokemon)
        }
    }

    /**
     * Helper method to get the max Hp of a Pokemon.
     * @param pokemonName name of the Pokemon.
     */
    private suspend fun returnPokemonHp(pokemonName:String): Int{
        loadPokemonByName(pokemonName, requireActivity().applicationContext)
        val pokemon = db.PokemonDao().getPokemonByName(pokemonName)!!
        return pokemon.maxHp
    }
}