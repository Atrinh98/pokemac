package dawsoncollege.android.pokemac.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dawsoncollege.android.pokemac.*
import dawsoncollege.android.pokemac.Database.*
import dawsoncollege.android.pokemac.databinding.FragmentPokemonBattleBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.random.Random

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
class PokemonBattleFragment : Fragment(R.layout.fragment_pokemon_battle) {

    // Fields
    private lateinit var binding: FragmentPokemonBattleBinding
    private lateinit var ally: Pokemon
    private lateinit var enemy: Pokemon
    private lateinit var db: PokemonDatabase
    private var userAlive: Boolean = true
    private var enemyAlive: Boolean = true

    /**
     * Creates the view and return the root binding (default function).
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokemonBattleBinding.inflate(layoutInflater)
        return binding.root
    }

    /**
     * Bindings are done here and the function runs from here (default function).
     * Load the enemy and ally Pokemon and checks if they are alive or not.
     * If enemy is not, then we win battle.
     * If ally is not, then we swap until one is above 0 hp.
     * If still not, then we lost the battle.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch(Dispatchers.IO){
            db = getDb(requireActivity().applicationContext)

            // Loading indicator for slow devices.
            withContext(Dispatchers.Main){
                binding.loadingIndicator.visibility = View.VISIBLE
                binding.bottom.visibility = View.GONE
            }

            // Load pokemon info
            userAlive = loadAlly()

            if (!userAlive){
                getNextAlly(requireContext().applicationContext)
                userAlive = loadAlly()
            }
            // Check level up and see if theres a new move.
            if (userAlive){
                withContext(Dispatchers.Main){
                    // Check to see if the pokemon leveled up or not
                    if (ally.level < getLevel(ally.experience)){
                        withContext(Dispatchers.IO){
                            val move = getPokemonNewMove(ally.species, getLevel(ally.experience))
                            println(move)
                            if (!move.isNullOrEmpty()){
                                withContext(Dispatchers.Main){
                                    val bundle = Bundle()
                                    bundle.putString("battleType", "pokemon")
                                    view.findNavController().navigate(R.id.action_pokemonBattleFragment_to_chooseMoveFragment, bundle)
                                }
                            }
                        }
                    }
                }
            }
            enemyAlive = loadEnemy()

            withContext(Dispatchers.Main){
                // If user or pokemon is not alive
                if (!userAlive || !enemyAlive) {
                    bindingMenu()
                }
                else{
                    bindingAlly()
                    bindingEnemy()
                    // If we move (swap, or bag) then enemy attacks
                    if (arguments?.getBoolean("move")!!){
                        withContext(Dispatchers.IO){
                            val dmg = attackAlly()
                            withContext(Dispatchers.Main){
                                showDamageMessage(dmg)
                                if (!userAlive){
                                    bindingMenu()
                                }
                                else{
                                    bindingAlly()
                                    bindingEvent()
                                }
                            }
                        }
                    }
                    // Choose what to do and we did not move
                    else{
                        bindingEvent()
                    }
                    // Load image later if device is slow
                    withContext(Dispatchers.IO){
                        // Load the image
                        val front = getBitmap(enemy.front)
                        val back = getBitmap(ally.back)
                        withContext(Dispatchers.Main){
                            binding.pokemonImage.setImageBitmap(back)
                            binding.enemyImage.setImageBitmap(front)
                        }
                    }
                }
            }
        }
    }

    //------------------------------Fighting Functions---------------------------------------//

    /**
     * Show the damage report from the enemy with a Toast.
     * @param dmg Damage or Healing done by the enemy Pokemon.
     */
    private suspend fun showDamageMessage(dmg: Int){
        // Pokemon healed
        if (dmg > 0){
            val message = resources.getString(R.string.enemy_attack_heal, dmg)
            showToast(message)
        }
        // Pokemon miss attack
        else if (dmg == 0){
            val message = resources.getString(R.string.enemy_attack_miss)
            showToast(message)
        }
        // Pokemon died
        else if (dmg == -9999){
            val message = resources.getString(R.string.enemy_attack_faint)
            showToast(message)
            withContext(Dispatchers.IO){
                val currentAlly = db.CollectionDao().getPokemon(1)!!
                currentAlly.current_hp = 0
                db.CollectionDao().updateCollection(currentAlly)
                getNextAlly(requireContext().applicationContext)
                userAlive = loadAlly()
            }
        }
        // Pokemon attack
        else{
            val message = resources.getString(R.string.enemy_attack_damage, dmg)
            showToast(message)
        }
    }

    /**
     * The toast to show the damage report.
     */
    private fun showToast(message: String) =
        Toast.makeText(requireActivity().applicationContext, message, Toast.LENGTH_SHORT).show()

    /**
     * Attack the ally Pokemon when the user moves (bag event or swap Pokemons).
     */
    private suspend fun attackAlly(): Int {
        val enemyInfo = db.CollectionDao().getPokemon(-1)!!
        var attack = false
        var moveName = ""
        var moveNumber = 0
        while(!attack){
            moveNumber = Random.nextInt(4)
            when (moveNumber) {
                1 -> {
                    if (enemyInfo.pp_1 != 0){
                        moveName = enemy.move1
                        attack = true
                    }
                }
                2 -> {
                    if (enemyInfo.pp_2 != 0){
                        moveName = enemy.move2
                        attack = true
                    }
                }
                3 -> {
                    if (enemyInfo.pp_3 != 0){
                        moveName = enemy.move3
                        attack = true
                    }
                }
                else -> {
                    if (enemyInfo.pp_4 != 0){
                        moveName = enemy.move4
                        attack = true
                    }
                }
            }
        }
        loadMove(moveName, requireActivity().applicationContext)
        val move = db.MoveDao().getMove(moveName)!!
        val dmg = move.attack(ally, enemy, requireContext().applicationContext)
        savePokemon(moveNumber, dmg)
        return dmg
    }

    /**
     * Save the information of the fight.
     * @param moveNumber number of the move that was used by the enemy Pokemon.
     * @param dmg healing or damage done by the enemy Pokemon.
     */
    private suspend fun savePokemon(moveNumber: Int, dmg: Int){
        val enemyDb = db.CollectionDao().getPokemon(-1)!!
        val allyDb = db.CollectionDao().getPokemon(1)!!

        when (moveNumber) {
            1 -> {
                enemyDb.pp_1 -= 1
            }
            2 -> {
                enemyDb.pp_2 -= 1
            }
            3 -> {
                enemyDb.pp_3 -= 1
            }
            else -> {
                enemyDb.pp_4 -= 1
            }
        }
        // Pokemon healed
        if (dmg > 0){
            enemyDb.current_hp = enemy.hp
            db.CollectionDao().updateCollection(enemyDb)
        }
        if (dmg == -9999){
            allyDb.current_hp = 0
        }
        // Pokemon attack
        else if (dmg < 0){
            allyDb.current_hp = ally.hp
            db.CollectionDao().updateCollection(enemyDb)
            db.CollectionDao().updateCollection(allyDb)
        }
    }

    //----------------- Load Pokemons + helper functions----------------------------//

    /**
     * Load the ally Pokemon and check if it alive or not.
     * @return Boolean to check if the ally Pokemon is alive left.
     */
    private suspend fun loadAlly(): Boolean{
        val allyDb = db.CollectionDao().getPokemon(1)!!
        // Ally is dead
        if (allyDb.current_hp == 0){
            // Show the user the he lost
            withContext(Dispatchers.Main){
                binding.menuText.text = resources.getString(R.string.battle_lost)
            }
            return false
        }
        // Load the pokemon
        else{
            ally = getPokemonCollection(allyDb, requireActivity().applicationContext)
            // Set the stat to match the level
            ally.setStatValues()
            if (allyDb.current_hp > ally.maxHp){
                allyDb.current_hp = ally.maxHp
                db.CollectionDao().updateCollection(allyDb)
            }
            ally.hp = allyDb.current_hp
            return true
        }
    }

    /**
     * Load the enemy Pokemon and check if it alive or not.
     * @return Boolean to check if the enemy Pokemon is alive left.
     */
    private suspend fun loadEnemy(): Boolean{
        val enemyDb = db.CollectionDao().getPokemon(-1)
        // No enemy take a random one
        if (enemyDb == null){
            enemy = getRandomPokemonObject(setEnemyLevel(requireContext().applicationContext),
                requireContext().applicationContext, -1)
            // Set the stat to match the level
            enemy.setStatValues()
            enemy.resetHp()
            return true
        }
        // Load the info needed from the enemy
        else{
            if (enemyDb.current_hp == 0){
                withContext(Dispatchers.Main){
                    binding.menuText.text = resources.getString(R.string.battle_won)
                }
                return false
            }
            else{
                enemy = getPokemonCollection(enemyDb, requireContext().applicationContext)
                enemy.setStatValues()
                if (enemyDb.current_hp > enemy.maxHp){
                    enemyDb.current_hp = enemy.maxHp
                    db.CollectionDao().updateCollection(enemyDb)
                }
                enemy.hp = enemyDb.current_hp
                return true
            }
        }
    }


    //-------------------------------- Bindings-------------------------------------//

    /**
     * Set the binding for the ally Pokemon.
     */
    private fun bindingAlly(){
        binding.pokemonName.text = ally.name
        binding.pokemonLvl.text = ally.level.toString()
        binding.pokemonHp.max = ally.maxHp
        binding.pokemonHp.progress = ally.hp
        binding.pokemonHpText.text = resources.getString(
            R.string.hp_text, ally.hp,
            ally.maxHp)
    }

    /**
     * Set the binding for the enemy Pokemon.
     */
    private fun bindingEnemy(){
        binding.enemyName.text = enemy.name
        binding.enemyLvl.text = enemy.level.toString()
        binding.enemyHp.max = enemy.maxHp
        binding.enemyHp.progress = enemy.hp
        binding.enemyHpText.text = resources.getString(
            R.string.hp_text, enemy.hp,
            enemy.maxHp)
    }

    /**
     * Set the binding for the menu option when battle is won or lost.
     */
    private fun bindingMenu(){
        binding.bottom.visibility = View.GONE
        binding.menu.visibility = View.VISIBLE
        binding.menuText.visibility = View.VISIBLE
        binding.top.visibility = View.INVISIBLE
        binding.loadingIndicator.visibility = View.GONE
        binding.menu.setOnClickListener {
            it.findNavController().navigate(R.id.action_pokemonBattleFragment_to_menuFragment)
        }
    }

    /**
     * Assigns event action to the button bindings.
     * Lets the user decide between: Fighting, Using the bag, Swapping or Running.
     */
    private fun bindingEvent(){
        binding.loadingIndicator.visibility = View.GONE
        binding.bottom.visibility = View.VISIBLE
        binding.fight.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("battleType", "pokemon")
            it.findNavController().navigate(R.id.action_pokemonBattleFragment_to_fightFragment, bundle)
        }
        binding.swap.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("battleType", "pokemon")
            it.findNavController().navigate(R.id.action_pokemonBattleFragment_to_swapFragment, bundle)
        }
        binding.bag.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("battleType", "pokemon")
            it.findNavController().navigate(R.id.action_pokemonBattleFragment_to_bagFragment, bundle)
        }
        binding.run.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("battleType", "pokemon")
            it.findNavController().navigate(R.id.action_pokemonBattleFragment_to_runFragment, bundle)
        }
    }
}