package dawsoncollege.android.pokemac

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dawsoncollege.android.pokemac.Database.PokedexEntity

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Dao
interface PokedexDAO {

    /**
     * Query to get all the Pokedex entries.
     * @return MutableList of every PokedexEntry.
     */
    @Query("Select * from PokedexEntity")
    suspend fun getAll(): MutableList<PokedexEntity>

    /**
     * Query to get specific Pokedex Entry with the same name.
     * @param species the species of the Pokedex entry.
     * @return PokedexEntry with the same name.
     */
    @Query("Select * from PokedexEntity where name = :species")
    suspend fun getPokedexEntryByName(species: String): PokedexEntity

    /**
     * Insert query to insert all Pokedex entries to database.
     * @param pokedexEntry the list of every Pokemon as PokedexEntity.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pokedexEntry: MutableList<PokedexEntity>)
}