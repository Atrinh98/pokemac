package dawsoncollege.android.pokemac.Database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Dao
interface MoveDao {

    /**
     * Insert query to add MoveEntity to the database.
     * @param move MoveEntity to add to the database.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMove(vararg move: MoveEntity)

    /**
     * Query to get the move with the same name.
     * @param name name of the move.
     * @return MoveEntity if it exist or else null.
     */
    @Query("Select * from MoveEntity where name = :name")
    suspend fun getMove(name: String): MoveEntity?
}