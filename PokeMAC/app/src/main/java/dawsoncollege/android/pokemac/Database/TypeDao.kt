package dawsoncollege.android.pokemac.Database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Dao
interface TypeDao {

    /**
     * Query to get the damage multiplier from the attacker type and defender type.
     * @param attack attacker's type.
     * @param defense defender's type.
     * @return the type multiplier.
     */
    @Query("Select damage from TypeEntity where attackType = :attack and defenseType =:defense")
    suspend fun getDamage(attack: String, defense: String): Double?

    /**
     * Query to check if the attacker type has already been added to database or not.
     * @param type attacker's type.
     * @return TypeEntity if it exists or else null.
     */
    @Query("Select * from TypeEntity where attackType = :type")
    suspend fun checkType(type: String): TypeEntity?

    /**
     * Insert query to insert the TypeEntity to the database.
     * @param type TypeEntity to be added to the database.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertType(vararg type: TypeEntity)
}