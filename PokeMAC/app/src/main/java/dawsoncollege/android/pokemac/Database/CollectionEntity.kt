package dawsoncollege.android.pokemac.Database

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import dawsoncollege.android.pokemac.Pokemon

/**
 * @author Andrew Trinh
 * @author Maedeh Hassani
 * @author Cuneyt Yildirim
 */
@Entity
data class CollectionEntity (
        @ColumnInfo(name="name") val name: String,
        @ColumnInfo(name="species") val species: String,
        @ColumnInfo(name="current_exp") var current_Exp: Int,
        @ColumnInfo(name = "current_hp") var current_hp: Int,
        @ColumnInfo(name= "level") var level: Int,
        @ColumnInfo(name="move_1") var move_1: String,
        @ColumnInfo(name="move_2") var move_2: String,
        @ColumnInfo(name="move_3") var move_3: String,
        @ColumnInfo(name="move_4") var move_4: String,
        @ColumnInfo(name="pp_1") var pp_1: Int,
        @ColumnInfo(name="pp_2") var pp_2: Int,
        @ColumnInfo(name="pp_3") var pp_3: Int,
        @ColumnInfo(name="pp_4") var pp_4: Int,
        @ColumnInfo(name="position") var position: Int,
        @PrimaryKey(autoGenerate = true)
        val collectionId: Int = 0
)

/**
 * Replace the first position of the team with the first Pokemon that was more than 0 hp.
 * @param context to have access to our database.
 */
suspend fun replaceFirst(context: Context){
        val db = getDb(context)
        val first = db.CollectionDao().getPokemon(1)
        val team = db.CollectionDao().getTeam()
        var swap = false
        if (first == null){
                for (pokemon in team)
                {
                        if (pokemon.current_hp > 0 && !swap){
                                pokemon.position = 1
                                db.CollectionDao().updateCollection(pokemon)
                                swap = true
                        }
                }
        }
}


/**
 * Pokemon will gain experience depending on the level of the enemy and the base reward of the poke api.
 * @param battleType pokemon or trainer to see the multiplier gain.
 * @param collectionEntity collection entry which will gain the experience.
 * @param enemy Pokemon which we take the level and base reward.
 * @param context to have access to our database.
 */
suspend fun gainXp(battleType: String, collectionEntity: CollectionEntity , enemy: Pokemon, context: Context) {
        val db = getDb(context)
        if (battleType == "pokemon") {
                collectionEntity.current_Exp += (0.3 * enemy.baseReward * enemy.level).toInt()
        }
        else {
                collectionEntity.current_Exp += (2 * (0.3 * enemy.baseReward * enemy.level)).toInt()
        }

        println("${collectionEntity.name} won xp")
        db.CollectionDao().updateCollection(collectionEntity)
}

/**
 * Load the next enemy if they are above 0 hp.
 * Mainly use for trainer battles.
 * @param context to have access to our database.
 */
suspend fun getNextEnemy(context: Context){
        val db = getDb(context)
        val dead = db.CollectionDao().getPokemon(-1)!!
        val team = db.CollectionDao().getEnemyTeam()
        var swap = false
        for (pokemon in team)
        {
                if (pokemon.current_hp > 0 && !swap){
                        dead.position = pokemon.position
                        pokemon.position = -1
                        db.CollectionDao().updateCollection(pokemon)
                        db.CollectionDao().updateCollection(dead)
                        swap = true
                }
        }
}


/**
 * Load the next ally that has more than 0 hp.
 * Mainly use for when the Pokemon at first position faints.
 * @param context to have access to our database.
 */
suspend fun getNextAlly(context: Context){
        val db = getDb(context)
        val dead = db.CollectionDao().getPokemon(1)!!
        val team = db.CollectionDao().getTeam()
        var swap = false
        for (pokemon in team)
        {
                if (pokemon.current_hp > 0 && !swap){
                        dead.position = pokemon.position
                        pokemon.position = 1
                        db.CollectionDao().updateCollection(pokemon)
                        db.CollectionDao().updateCollection(dead)
                        swap = true
                }
        }
}