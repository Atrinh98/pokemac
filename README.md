# pokemac

## Demo

https://www.youtube.com/watch?v=CoHtONdEiIY

## Members

- Andrew Trinh
- Maedeh Hassani
- Cuneyt Yildirim

## Description

An Android application where you get to play Pokemon
Pokemon Application where you get to battle/capture Pokemons
Fight other trainers
Change your team
Heal your team
This application is battle simulator just like Pokemon

## Technology Stack

- Kotlin
- Android Studio
- PokeAPI

## How to Run

Open the PokeMAC project folder in Android Studio
Plug in your Android device or download and Emulator

1. Once Android device or Emulator is loaded
2. Press play and this will start PokeMAC application

## Usage

Once the application starts you will be able to press buttons
If it is your first time
    - It will prompt you to enter your trainer and choose your starter Pokemon
    - Then you will proceed like usual
If it is not you will play the game like usual
You will have a choice between what you want to do in the game
    - Play the game
        - Against a Pokemon
            - Defeat or capture the other Pokemon to win the battle
        - Against a trainer
            - Fight against a number of a trainer's Pokemon, the first to defeat all other adversary Pokemon wins
    - Change your team
        - Swap between Pokemon in your box or team itself
    - Pokecenter
        - Restore all your Pokemon's HP and PP in your team to max
    - Save
        - Does not do anything

When in battle you can:
    - Fight
        - Choose your current Pokemon's move and attack the opposing
    - Bag
        - Heal your Pokemon
        - Have a chance to catch opposing opposing Pokemon (Wild only)
    - Swap
        - Change your fighting Pokemon with a resting one
    - Run (Wild only)
        - Fkee from the battle and return to the main menu

When you are done playing, close the application or press the 'X (Stop)' button on Android Studio. 

## Credits

https://pokeapi.co/